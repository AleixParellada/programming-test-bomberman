
GAME CONTROLS
-------------
Player 1

- Movement: WASD.
- Bomb Placing: F.
- Bomb Detonation: G.

Player 2:
- Movement: Keyboard Arrows.
- Bomb Placing: K.
- Bomb Detonation: L.

-------------
TIME SPENT
-------------
25+ hours.

-------------
LINK TO REPOSITORY
-------------
https://gitlab.com/AleixParellada/programming-test-bomberman
