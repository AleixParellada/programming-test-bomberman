#include "Bomb.h"
#include "MapGrid.h"
#include "GridTile.h"
#include "BombExplosionFire.h"
#include "EGridTileTypes.h"

ABomb::ABomb()
{
	PrimaryActorTick.bCanEverTick = false;
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CustomStaticMesh"));
	RootComponent = StaticMeshComponent;
	
	bombTimeToExplode = 2.f;
	bombExplosionDuration = 2.f;
}

void ABomb::Initialize(AMapGrid* Map, AGridTile* GridTile, int explosionPower, bool isRemotelyDetonated)
{
	MapGrid = Map;
	CurrentGridTile = GridTile;
	bombExplosionPower = explosionPower;
	isBombRemotelyDetonated = isRemotelyDetonated;
	if (!isBombRemotelyDetonated)
	{
		GetWorldTimerManager().SetTimer(ExplodeBombTimerHandle, this, &ABomb::ExplodeBomb, bombTimeToExplode, false, bombTimeToExplode);
	}

	BombExplosionFireBPClass = Cast<UClass>(StaticLoadObject(UClass::StaticClass(), NULL, TEXT("Blueprint'/Game/Blueprints/BP_BombExplosionFire.BP_BombExplosionFire_C'")));
	check(BombExplosionFireBPClass)
}

void ABomb::ExplodeBomb()
{
	hasBombExploded = true;
	BombExplosionDelegate.Broadcast();
	GetWorldTimerManager().SetTimer(BombExplosionTimerHandle, this, &ABomb::DestroyActor, bombExplosionDuration, false, bombExplosionDuration);

	TArray<AGridTile*> gridTilesAffected;
	GetGridTilesAffected(gridTilesAffected);
	for (AGridTile* GridTile : gridTilesAffected)
	{
		GridTile->OnAffectedByBombExplosion();
		CreateBombExplosionFireAtGridTile(GridTile);
	}

	if (StaticMeshComponent)
	{
		StaticMeshComponent->DestroyComponent();
		StaticMeshComponent = nullptr;
	}

	BombExplosionDelegate.Clear();
	GetWorldTimerManager().ClearTimer(ExplodeBombTimerHandle);
}

void ABomb::GetGridTilesAffected(TArray<AGridTile*>& gridTilesAffected)
{
	gridTilesAffected.Add(CurrentGridTile);
	for (int tileDirection = 0; tileDirection < (int)ETileDirectionsToCheck::COUNT; tileDirection++)
	{
		AGridTile* GridTile = CurrentGridTile;
		for (int i = 0; i < bombExplosionPower; i++)
		{
			GridTile = MapGrid->GetAdjacentGridTileAtDirection(GridTile, (ETileDirectionsToCheck)tileDirection);
			if (GridTile && !GridTile->IsUnbreakable())
			{
				gridTilesAffected.Add(GridTile);
			}
			else
			{
				break;
			}
		}
	}
}

void ABomb::CreateBombExplosionFireAtGridTile(AGridTile* GridTile)
{
	FRotator tileRotation = FRotator(0.f);
	GetBombExplosionFireRotation(GridTile, tileRotation);

	ABombExplosionFire* BombExplosionFire = GetWorld()->SpawnActor<ABombExplosionFire>(BombExplosionFireBPClass, FVector(GridTile->GetTileGridRelativePosition(), 5.f), tileRotation, FActorSpawnParameters());
	check(BombExplosionFire)
	BombExplosionFire->BombExplosionFireOverlapPlayerDelegate.AddDynamic(this, &ABomb::OnBombExplosionFireOverlapPlayer);
	BombExplosionFire->Initialize();

	BombExplosionFireList.Add(BombExplosionFire);
}

void ABomb::GetBombExplosionFireRotation(AGridTile* GridTile, FRotator& desiredRotation)
{
	bool applyNewRotation = false;
	if (GridTile != CurrentGridTile)
	{
		FIntPoint directionToCurrent = GridTile->GetTileGridPoint() - CurrentGridTile->GetTileGridPoint();
		applyNewRotation = directionToCurrent.Y == 0;
	}
	else
	{
		AGridTile* LeftGridTile = MapGrid->GetAdjacentGridTileAtDirection(CurrentGridTile, ETileDirectionsToCheck::LEFT);
		AGridTile* RightGridTile = MapGrid->GetAdjacentGridTileAtDirection(CurrentGridTile, ETileDirectionsToCheck::RIGHT);
		applyNewRotation = (LeftGridTile && !LeftGridTile->IsUnbreakable()) || (RightGridTile && !RightGridTile->IsUnbreakable());
	}

	desiredRotation.Yaw = applyNewRotation ? 90.f : 0.f;
}

void ABomb::OnBombExplosionFireOverlapPlayer(AMainCharacter* OverlappingActor)
{
	if(OverlappingActor)
	{
		BombFireOverlapPlayerDelegate.Broadcast(OverlappingActor);
	}
}

void ABomb::DestroyActor()
{
	if (BombExplosionFireList.GetData())
	{
		for (ABombExplosionFire* FireStaticMeshComponent : BombExplosionFireList)
		{
			FireStaticMeshComponent->DestroyActor();
			FireStaticMeshComponent = nullptr;
		}
		BombExplosionFireList.Empty();
	}

	if (StaticMeshComponent)
	{
		StaticMeshComponent->DestroyComponent();
		StaticMeshComponent = nullptr;
	}

	MapGrid = nullptr;
	CurrentGridTile = nullptr;
	BombExplosionFireBPClass = nullptr;

	BombExplosionDelegate.Clear();
	BombFireOverlapPlayerDelegate.Clear();
	GetWorldTimerManager().ClearTimer(ExplodeBombTimerHandle);
	GetWorldTimerManager().ClearTimer(BombExplosionTimerHandle);

	this->RemoveFromRoot();
	this->Destroy();
}
