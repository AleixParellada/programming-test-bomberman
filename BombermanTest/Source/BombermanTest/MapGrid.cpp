#include "MapGrid.h"
#include "GridTile.h"
#include "EGridTileTypes.h"

AMapGrid::AMapGrid()
{
	PrimaryActorTick.bCanEverTick = false;
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CustomStaticMesh"));
	RootComponent = StaticMeshComponent;
}

void AMapGrid::BeginPlay()
{
	Super::BeginPlay();
	CreateGrid();
}

void AMapGrid::CreateGrid()
{
	for (int i = 0; i < GRID_NM_DIMENSIONS; i++)
	{
		for (int j = 0; j < GRID_NM_DIMENSIONS; j++)
		{
			FIntPoint gridTilePoint(i, j);
			FillGridWithTiles(gridTilePoint);
		}
	}

	MapGridFilledDelegate.Broadcast();
}

void AMapGrid::FillGridWithTiles(FIntPoint gridTilePoint)
{
	AGridTile* GridTile = GetWorld()->SpawnActor<AGridTile>(FVector(0.f), FRotator(0.f), FActorSpawnParameters());
	EGridTileTypes EGridTileType = CalculateGridTileType(gridTilePoint);
	GridTile->Initialize(EGridTileType, StaticMeshComponent, gridTilePoint);
	
	if (EGridTileType == EGridTileTypes::STARTING_POINT)
	{
		StartingGridTilesList.Add(GridTile);
	}
	GridTilesList.Add(GridTile);
}

// Breakable tiles are created with a hard-coded 65% chance.
EGridTileTypes AMapGrid::CalculateGridTileType(FIntPoint gridTilePoint)
{
	EGridTileTypes TileType;

	if (IsStartingGridTilePosition(gridTilePoint))
	{
		TileType = EGridTileTypes::STARTING_POINT;
	}
	else if (IsGridTilePositionAdjacentToStartingPoint(gridTilePoint))
	{
		TileType = EGridTileTypes::EMPTY;
	}
	else if (gridTilePoint.X % 2 == 0 || gridTilePoint.Y % 2 == 0)
	{
		TileType = FMath::RandRange(0, 100) < 65 ? EGridTileTypes::BREAKABLE : EGridTileTypes::EMPTY;
	}
	else
	{
		TileType = EGridTileTypes::UNBREAKABLE;
	}

	return TileType;
}

bool AMapGrid::IsStartingGridTilePosition(FIntPoint gridTilePoint)
{
	bool isStartingGridTilePosition = false;

	if (gridTilePoint.X == GRID_NM_DIMENSIONS - 1 || gridTilePoint.X == 0)
	{
		isStartingGridTilePosition = gridTilePoint.Y == 0 || gridTilePoint.Y == GRID_NM_DIMENSIONS - 1;
	}
	else if (gridTilePoint.Y == GRID_NM_DIMENSIONS - 1 || gridTilePoint.Y == 0)
	{
		isStartingGridTilePosition = gridTilePoint.X == 0 || gridTilePoint.X == GRID_NM_DIMENSIONS - 1;
	}

	return isStartingGridTilePosition;
}

// Checking if given position is adjacent to the starting point, to leave optimal space to the player to be able to drop a bomb without killing himself for not having a place to hide. This is to avoid unbeatable generated maps.
bool AMapGrid::IsGridTilePositionAdjacentToStartingPoint(FIntPoint gridTilePoint)
{
	bool isAdjacent = false;

	if (gridTilePoint.X == 0 || gridTilePoint.X == GRID_NM_DIMENSIONS - 1)
	{
		isAdjacent = gridTilePoint.Y == 1 || gridTilePoint.Y == GRID_NM_DIMENSIONS - 2;
	}
	else if (gridTilePoint.X == 1 || gridTilePoint.X == GRID_NM_DIMENSIONS - 2)
	{
		isAdjacent = gridTilePoint.Y == 0 || gridTilePoint.Y == GRID_NM_DIMENSIONS - 1;
	}

	return isAdjacent;
}

AGridTile* AMapGrid::GetAdjacentGridTileAtDirection(AGridTile* ReferenceGridTile, ETileDirectionsToCheck DirectionToCheck)
{
	FIntPoint referenceGridTilePoint = ReferenceGridTile->GetTileGridPoint();
	switch (DirectionToCheck)
	{
		case ETileDirectionsToCheck::LEFT: referenceGridTilePoint.X -= 1; break;
		case ETileDirectionsToCheck::RIGHT: referenceGridTilePoint.X += 1; break;
		case ETileDirectionsToCheck::UP: referenceGridTilePoint.Y -= 1; break;
		case ETileDirectionsToCheck::DOWN: referenceGridTilePoint.Y += 1; break;
	}

	return GetGridTileAtPoint(referenceGridTilePoint);
}

AGridTile* AMapGrid::GetGridTileAtPoint(FIntPoint gridTilePoint)
{
	AGridTile* GridTile = nullptr;

	if(!IsGridTilePointOutsideMap(gridTilePoint))
	{ 
		int tileIndex = gridTilePoint.X * GRID_NM_DIMENSIONS + gridTilePoint.Y;
		if (tileIndex < GridTilesList.Num())
		{
			GridTile = GridTilesList[tileIndex];
		}
	}

	return GridTile;
}

bool AMapGrid::IsGridTilePointOutsideMap(FIntPoint gridTilePoint)
{
	return gridTilePoint.X < 0 || gridTilePoint.Y < 0 || gridTilePoint.X > GRID_NM_DIMENSIONS - 1 || gridTilePoint.Y > GRID_NM_DIMENSIONS - 1;
}

void AMapGrid::ResetActor()
{
	for (AGridTile* GridTile : GridTilesList)
	{
		GridTile->DestroyActor();
		GridTile = nullptr;
	}
	GridTilesList.Empty();
	
	for (AGridTile* StartingGridTile : StartingGridTilesList)
	{
		if (StartingGridTile)
		{
			StartingGridTile->DestroyActor();
			StartingGridTile = nullptr;
		}
	}
	StartingGridTilesList.Empty();
	CreateGrid();
}