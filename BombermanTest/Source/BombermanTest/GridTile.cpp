#include "GridTile.h"
#include "PowerUpItem.h"
#include "Bomb.h"

AGridTile::AGridTile()
{
	PrimaryActorTick.bCanEverTick = false;
}

void AGridTile::Initialize(EGridTileTypes GridType, UStaticMeshComponent* ParentStaticMeshComponent, FIntPoint gridPoint)
{
	GridTileType = GridType;
	tileGridPoint = gridPoint;
	
	if (UPrimitiveComponent* PrimitiveComponent = this->FindComponentByClass<UPrimitiveComponent>())
	{
		PrimitiveComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	}

	switch (GridType)
	{
		default:
		case EGridTileTypes::EMPTY: PlacedBrick = NewObject<UGridEmptyMeshTile>(UGridEmptyMeshTile::StaticClass()); break;
		case EGridTileTypes::STARTING_POINT: PlacedBrick = NewObject<UGridStartingPointMeshTile>(UGridStartingPointMeshTile::StaticClass()); break;
		case EGridTileTypes::BREAKABLE: PlacedBrick = NewObject<UGridBreakableBrickMeshTile>(UGridBreakableBrickMeshTile::StaticClass()); break;
		case EGridTileTypes::UNBREAKABLE: PlacedBrick = NewObject<UGridUnbreakableBrickMeshTile>(UGridUnbreakableBrickMeshTile::StaticClass());  break;
	}

	PlacedBrick->AttachTo(ParentStaticMeshComponent);
	tileGridRelativePosition = PlacedBrick->CreateTile(ParentStaticMeshComponent->GetStaticMesh(), tileGridPoint);
	PlacedBrick->RegisterComponentWithWorld(GetWorld());
}

void AGridTile::OnAffectedByBombExplosion()
{
	DestroyPlacedPowerUpItem();
	ExplodePlacedBreakableBrick();
	ExplodePlacedBomb();
}

void AGridTile::DestroyPlacedPowerUpItem()
{
	if (PlacedPowerUpItem)
	{
		PlacedPowerUpItem->DestroyActor();
		PlacedPowerUpItem = nullptr;
	}
}

void AGridTile::ExplodePlacedBreakableBrick()
{
	if (!PlacedBrick || GridTileType != EGridTileTypes::BREAKABLE)
	{
		return;
	}
	else if (FMath::RandRange(0, 100) <= 30)
	{
		CreatePowerUpItem();
	}

	DestroyPlacedStaticMesh();
	GridTileType = EGridTileTypes::EMPTY;
}

void AGridTile::CreatePowerUpItem()
{
	UClass* PowerUpBPClass = Cast<UClass>(StaticLoadObject(UClass::StaticClass(), NULL, TEXT("/Game/Blueprints/BP_PowerUpItem.BP_PowerUpItem_C")));
	check(PowerUpBPClass)

	PlacedPowerUpItem = GetWorld()->SpawnActor<APowerUpItem>(PowerUpBPClass, FVector(GetTileGridRelativePosition(), 1.f), FRotator(0.f), FActorSpawnParameters());
	PlacedPowerUpItem->Initialize((EPowerUpType)FMath::RandRange(0, (int)EPowerUpType::COUNT - 1));
}

void AGridTile::DestroyPlacedStaticMesh()
{
	if (PlacedBrick)
	{
		PlacedBrick->DestroyActor();
		PlacedBrick = nullptr;
	}
}

void AGridTile::DestroyPlacedBomb()
{
	if (PlacedBomb)
	{
		PlacedBomb->DestroyActor();
		PlacedBomb = nullptr;
	}
}

void AGridTile::ExplodePlacedBomb()
{
	if (PlacedBomb && !PlacedBomb->HasBombExploded())
	{
		PlacedBomb->ExplodeBomb();
	}
	PlacedBomb = nullptr;
}

void AGridTile::InjectBomb(ABomb* bomb)
{
	PlacedBomb = bomb;
}

void AGridTile::DestroyActor()
{
	DestroyPlacedBomb();
	DestroyPlacedPowerUpItem();
	DestroyPlacedStaticMesh();

	this->RemoveFromRoot();
	this->Destroy();
}



UGridEmptyMeshTile::UGridEmptyMeshTile()
{
	tileElevation = 5.f;
	tileScale = 0.9f;
	tileSize = 100.f;

	this->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
}

FVector2D UGridEmptyMeshTile::CreateTile(UStaticMesh* MapFloorStaticMesh, FIntPoint tileGridPos)
{
	FVector2D tileRelativePosition = FVector2D(0);
	check(MapFloorStaticMesh);
	
	float mapLimitSize = MapFloorStaticMesh->GetBounds().GetBox().GetSize().X * 0.5f;
	float scaledTileSize = tileSize * tileScale;
	tileRelativePosition = FVector2D(scaledTileSize * tileGridPos.X - (mapLimitSize - scaledTileSize * 0.5f), scaledTileSize * tileGridPos.Y - (mapLimitSize - scaledTileSize * 0.5f));

	if (TileStaticMesh)
	{
		this->SetStaticMesh(TileStaticMesh);
		this->SetWorldScale3D(FVector(tileScale));
		this->SetRelativeLocation(FVector(tileRelativePosition, tileElevation));
	}

	return tileRelativePosition;
}

void UGridEmptyMeshTile::DestroyActor()
{
	TileStaticMesh = nullptr;
	
	this->RemoveFromRoot();
	this->DestroyComponent();
}

UGridUnbreakableBrickMeshTile::UGridUnbreakableBrickMeshTile()
{
	ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMeshFinder(TEXT("StaticMesh'/Game/Assets/GridMap/Tiles/Unbreakable_Tile_Cube.Unbreakable_Tile_Cube'"));
	check(StaticMeshFinder.Object)
	TileStaticMesh = StaticMeshFinder.Object;
}

UGridBreakableBrickMeshTile::UGridBreakableBrickMeshTile()
{
	ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMeshFinder(TEXT("StaticMesh'/Game/Assets/GridMap/Tiles/Breakable_Tile_Cube.Breakable_Tile_Cube'"));
	check(StaticMeshFinder.Object)
	TileStaticMesh = StaticMeshFinder.Object;
}

UGridStartingPointMeshTile::UGridStartingPointMeshTile()
{
	tileElevation = 1.f;

	ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMeshFinder(TEXT("StaticMesh'/Game/Assets/GridMap/Tiles/StartPosition_Tile_Plane.StartPosition_Tile_Plane'"));
	check(StaticMeshFinder.Object)
	TileStaticMesh = StaticMeshFinder.Object;
}
