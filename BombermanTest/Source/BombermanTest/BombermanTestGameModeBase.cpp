#include "BombermanTestGameModeBase.h"

#include "MapGrid.h"
#include "GridTile.h"
#include "Kismet/GameplayStatics.h"

void ABombermanTestGameModeBase::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);
	
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
	
	initialMatchTime = 120.f;
	matchTimeLeft = initialMatchTime;
	Initialize();
}

void ABombermanTestGameModeBase::StartGame(int players)
{
}

void ABombermanTestGameModeBase::Initialize()
{
	LoadCharactersMaterial();
	SpawnMapGrid();
	SpawnPlayers();
}

void ABombermanTestGameModeBase::LoadCharactersMaterial()
{
	UMaterial* Material = Cast<UMaterial>(StaticLoadObject(UMaterial::StaticClass(), NULL, TEXT("Material'/Game/Assets/Bomberman/Materials/Black_Material.Black_Material'")));
	check(Material)
	CharactersMaterial.Add(Material);

	Material = Cast<UMaterial>(StaticLoadObject(UMaterial::StaticClass(), NULL, TEXT("Material'/Game/Assets/GridMap/Floor/Materials/GridMap_Material.GridMap_Material'")));
	check(Material)
	CharactersMaterial.Add(Material);

	Material = Cast<UMaterial>(StaticLoadObject(UMaterial::StaticClass(), NULL, TEXT("Material'/Game/Assets/Bomberman/Materials/Pink_Material.Pink_Material'")));
	check(Material)
	CharactersMaterial.Add(Material);
}

void ABombermanTestGameModeBase::SpawnMapGrid()
{
	UClass* MapGridBPClass = Cast<UClass>(StaticLoadObject(UClass::StaticClass(), NULL, TEXT("Blueprint'/Game/Blueprints/BP_MapGrid.BP_MapGrid_C'")));
	check(MapGridBPClass)
	
	MapGrid = GetWorld()->SpawnActor<AMapGrid>(MapGridBPClass, FVector(0.f), FRotator(0.f), FActorSpawnParameters());
	MapGrid->MapGridFilledDelegate.AddDynamic(this, &ABombermanTestGameModeBase::OnMapGridCreated);
}

void ABombermanTestGameModeBase::SpawnPlayers()
{
	UClass* MainCharacterBPClass = Cast<UClass>(StaticLoadObject(UClass::StaticClass(), NULL, TEXT("Blueprint'/Game/Blueprints/BP_MainCharacter.BP_MainCharacter_C'")));
	check(MainCharacterBPClass)

	FVector playersInitialLocation = FVector(0.f, 0.f, 10.f);
	if (numPlayers > 1)
	{
		Player_2 = GetWorld()->SpawnActor<AMainCharacter>(MainCharacterBPClass, playersInitialLocation, FRotator(0.f, 180.f, 0.f), FActorSpawnParameters());
		Player_2->SetSkeletalMeshMaterial(CharactersMaterial[(int)EPlayersIndex::PLAYER_1]);
	}
	Player_1 = GetWorld()->SpawnActor<AMainCharacter>(MainCharacterBPClass, playersInitialLocation, FRotator(0.f), FActorSpawnParameters());
}

void ABombermanTestGameModeBase::OnMapGridCreated()
{
	TArray<AGridTile*> startingTilesList;
	MapGrid->GetStartingTilesList(startingTilesList);

	Player_1->Initialize(MapGrid, startingTilesList[0]);
	Player_1->PlayerDeathDelegate.AddDynamic(this, &ABombermanTestGameModeBase::OnPlayerDied);
	if (Player_2)
	{
		Player_2->Initialize(MapGrid, startingTilesList[2]);
		Player_2->PlayerDeathDelegate.AddDynamic(this, &ABombermanTestGameModeBase::OnPlayerDied);
		Player_1->InjectSecondPlayer(Player_2);
	}

	GetWorldTimerManager().SetTimer(MatchEndTimer, this, &ABombermanTestGameModeBase::OnPlayerDeathTimerEnds, matchTimeLeft, false, matchTimeLeft);
}

void ABombermanTestGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetWorldTimerManager().IsTimerActive(MatchEndTimer))
	{
		matchTimeLeft -= DeltaTime;
	}
}

void ABombermanTestGameModeBase::OnPlayerDied()
{
	if (!GetWorldTimerManager().IsTimerActive(PlayerDeathTimer))
	{
		GetWorldTimerManager().SetTimer(PlayerDeathTimer, this, &ABombermanTestGameModeBase::OnPlayerDeathTimerEnds, 1.f, false, 1.f);
	}
}

void ABombermanTestGameModeBase::OnPlayerDeathTimerEnds()
{
	if (onMatchEnded)
	{
		return;
	}
	onMatchEnded = true;

	GetWorldTimerManager().ClearTimer(PlayerDeathTimer);
	GetWorldTimerManager().ClearTimer(MatchEndTimer);

	int winnerPlayerSchore = 0;
	AMainCharacter* Winner = nullptr;
	if (!Player_1->HasPlayerOverlappedWithFire() && Player_2->HasPlayerOverlappedWithFire())
	{
		WinnerPlayer = Player_1;
		winnerPlayerSchore = Player_1->GetCharacterScore();
	}
	else if(Player_1->HasPlayerOverlappedWithFire() && !Player_2->HasPlayerOverlappedWithFire())
	{
		WinnerPlayer = Player_2;
		winnerPlayerSchore = Player_2->GetCharacterScore();
	}

	if (winnerPlayerSchore > highScore)
	{
		highScore = winnerPlayerSchore;
	}

	Player_1->HidePlayerOnFireExplosionTrigger();
	Player_2->HidePlayerOnFireExplosionTrigger();

	EndGameDelegate.Broadcast();
}

void ABombermanTestGameModeBase::ResetGame()
{
	matchTimeLeft = initialMatchTime;
	onMatchEnded = false;

	WinnerPlayer = nullptr;
	Player_1->ResetActor();
	Player_2->ResetActor();
	
	MapGrid->ResetActor();
}

