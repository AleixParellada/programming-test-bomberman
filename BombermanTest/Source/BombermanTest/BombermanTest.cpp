// Copyright Epic Games, Inc. All Rights Reserved.

#include "BombermanTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, BombermanTest, "BombermanTest" );
