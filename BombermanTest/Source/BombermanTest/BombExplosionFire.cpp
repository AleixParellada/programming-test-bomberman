#include "BombExplosionFire.h"
#include "MainCharacter.h" 

ABombExplosionFire::ABombExplosionFire()
{
	PrimaryActorTick.bCanEverTick = false;
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CustomStaticMesh"));
	RootComponent = StaticMeshComponent;
}

void ABombExplosionFire::Initialize()
{
	LookForOverlappingActors();
	OnActorBeginOverlap.AddDynamic(this, &ABombExplosionFire::OnOverlap);
}

void ABombExplosionFire::LookForOverlappingActors()
{
	TArray<AActor*> OverlappingActors;
	GetOverlappingActors(OverlappingActors);
	for (AActor* OverlappingActor : OverlappingActors)
	{
		OnOverlap(nullptr, OverlappingActor);
	}
}

void ABombExplosionFire::OnOverlap(AActor* ThisActor, AActor* OverlappingActor)
{
	if (AMainCharacter* MainCharacter = Cast<AMainCharacter>(OverlappingActor))
	{
		BombExplosionFireOverlapPlayerDelegate.Broadcast(MainCharacter);
	}
}

void ABombExplosionFire::DestroyActor()
{
	if (StaticMeshComponent)
	{
		StaticMeshComponent->DestroyComponent();
		StaticMeshComponent = nullptr;
	}
	
	OnActorBeginOverlap.Clear();

	this->RemoveFromRoot();
	this->Destroy();
}
