#include "PowerUpItem.h"

APowerUpItem::APowerUpItem()
{
	PrimaryActorTick.bCanEverTick = false;
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("CustomStaticMesh"));
	RootComponent = StaticMeshComponent;
}

void APowerUpItem::Initialize(EPowerUpType Type)
{
	PowerUpType = Type;
	SetPowerUpMaterial();
}

void APowerUpItem::SetPowerUpMaterial()
{
	check(PowerUpType != EPowerUpType::COUNT);
	FString MaterialName = "";
	switch (PowerUpType)
	{
		case EPowerUpType::LONG_BOMB_BLAST: MaterialName = "/Game/Assets/PowerUps/Materials/Long_Bomb_Blast_Material.Long_Bomb_Blast_Material"; break;
		case EPowerUpType::MORE_BOMBS: MaterialName = "/Game/Assets/PowerUps/Materials/More_Bombs_Material.More_Bombs_Material"; break;
		case EPowerUpType::FAST_RUN_SPEED: MaterialName = "/Game/Assets/PowerUps/Materials/Fast_Run_Speed_Material.Fast_Run_Speed_Material"; break;
		case EPowerUpType::REMOTE_CONTROLLED_BOMBS: MaterialName = "/Game/Assets/PowerUps/Materials/Remote_Controlled_Bombs_Material.Remote_Controlled_Bombs_Material"; break;
	}

	UMaterial* PowerUpMaterial = Cast<UMaterial>(StaticLoadObject(UMaterial::StaticClass(), NULL, *MaterialName));
	check(PowerUpMaterial);
	StaticMeshComponent->SetMaterial(0, PowerUpMaterial);
}

void APowerUpItem::DestroyActor()
{
	if (StaticMeshComponent)
	{
		StaticMeshComponent->DestroyComponent();
		StaticMeshComponent = nullptr;
	}

	this->RemoveFromRoot();
	this->Destroy();
}
