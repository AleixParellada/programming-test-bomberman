#pragma once

enum class EGridTileTypes
{
	EMPTY,
	STARTING_POINT,
	BREAKABLE,
	UNBREAKABLE
};

enum class ETileDirectionsToCheck
{
	LEFT,
	RIGHT,
	UP,
	DOWN,
	COUNT
};

enum class EPowerUpType
{
	LONG_BOMB_BLAST,
	MORE_BOMBS,
	FAST_RUN_SPEED,
	REMOTE_CONTROLLED_BOMBS,
	COUNT
};