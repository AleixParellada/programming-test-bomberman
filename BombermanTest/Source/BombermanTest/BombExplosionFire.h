#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BombExplosionFire.generated.h"

class AMainCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBombExplosionFireOverlapPlayerDelegate, AMainCharacter*, OverlappingActor);

UCLASS()
class BOMBERMANTEST_API ABombExplosionFire : public AActor
{
	GENERATED_BODY()

public:
	ABombExplosionFire();
	void Initialize();
	void DestroyActor();

	UFUNCTION()
	void OnOverlap(AActor* ThisActor, AActor* OverlappingActor);

private:
	void LookForOverlappingActors();

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "ActorMeshComponent")
	UStaticMeshComponent* StaticMeshComponent = nullptr;

	FBombExplosionFireOverlapPlayerDelegate BombExplosionFireOverlapPlayerDelegate;
};
