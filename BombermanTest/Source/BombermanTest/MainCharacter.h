#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MainCharacter.generated.h"

class AGridTile;
class AMapGrid;
class UInputComponent;
class ABomb;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FPlayerDeathDelegate);

UCLASS()
class BOMBERMANTEST_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AMainCharacter();
	void Initialize(AMapGrid* Map, AGridTile* Current);
	void SetSkeletalMeshMaterial(UMaterial* MaterialToApply);
	void InjectSecondPlayer(AMainCharacter* Player);
	virtual void Tick(float DeltaTime) override;
	void HidePlayerOnFireExplosionTrigger();
	void ResetActor();

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	void ReceiveMovementInput(FIntPoint direction, bool isPlayer1);
	void ProcessMovementInput(FIntPoint direction);
	void ReceiveDropBombInput(bool isPlayer1);
	void DropBomb();
	void ReceiveDetonateBombInput(bool isPlayer1);
	void DetonateBombs();

	bool HasRemoteBombPowerUpActive() { return isRemoteBombPowerUpActive; }
	bool HasPlayerOverlappedWithFire() { return hasPlayerOverlappedWithFire; }
	int GetCharacterScore() { return characterScore; }
	int GetHasPendantMovement() { return hasPendantMovement; }

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnBombExploded();
	UFUNCTION()
	void OnFireOverlappedActor(AMainCharacter* OverlappingActor);

protected:
	virtual void BeginPlay();

private:
	void OnRemoteBombPowerUpExpires();
	void RotateCharacter(FIntPoint direction);
	void MoveCharacter(float speed);

public:
	DECLARE_DELEGATE_OneParam(FDropBombDelegate, bool);
	DECLARE_DELEGATE_TwoParams(FMoveCharacterDelegate, FIntPoint, bool);
	
	FPlayerDeathDelegate PlayerDeathDelegate;

	UPROPERTY(EditAnywhere, Category = "Movement")
	float movementSpeed = 0.f;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
	float movementSpeedToIncrease = 0.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BombMechanics")
	int numBombs = 0;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BombMechanics")
	int availableBombs = 0;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "BombMechanics")
	float remoteBombPowerUpDuration = 0.f;

private:
	bool isRemoteBombPowerUpActive = false;
	bool hasPlayerOverlappedWithFire = false;
	int bombExplosionPower = 0;
	int characterScore = 0;

	AMainCharacter* Player_2 = nullptr;
	UInputComponent* CharacterInputComponent = nullptr;

	AMapGrid* MapGrid = nullptr;
	AGridTile* CurrentGridTile = nullptr;
	AGridTile* DesiredMovementGridTile = nullptr;
	bool hasPendantMovement = false;

	ABomb* RemoteControlledBomb = nullptr;
	FTimerHandle BombPowerUpDurationTimer;

};
