#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EGridTileTypes.h"
#include "GridTile.generated.h"

class ABomb;
class APowerUpItem;

UCLASS()
class BOMBERMANTEST_API AGridTile : public AActor
{
	GENERATED_BODY()

public:
	AGridTile();
	void Initialize(EGridTileTypes GridType, UStaticMeshComponent* ParentStaticMeshComponent, FIntPoint gridPoint);
	void InjectBomb(ABomb* bomb);
	void OnAffectedByBombExplosion();
	void DestroyActor();

	FIntPoint GetTileGridPoint() { return tileGridPoint; }
	FVector2D GetTileGridRelativePosition() { return tileGridRelativePosition; }
	bool IsUnbreakable() { return GridTileType == EGridTileTypes::UNBREAKABLE; }
	bool IsWalkable() { return !PlacedBomb && (GridTileType == EGridTileTypes::EMPTY || GridTileType == EGridTileTypes::STARTING_POINT); }

private:
	void CreatePowerUpItem();
	void DestroyPlacedPowerUpItem();
	void ExplodePlacedBreakableBrick();
	void ExplodePlacedBomb();
	
	void DestroyPlacedBomb();
	void DestroyPlacedStaticMesh();

private:
	EGridTileTypes GridTileType = EGridTileTypes::EMPTY;
	FIntPoint tileGridPoint = FIntPoint(0);
	FVector2D tileGridRelativePosition = FVector2D(0.f);

	UGridEmptyMeshTile* PlacedBrick = nullptr;
	ABomb* PlacedBomb = nullptr;
	APowerUpItem* PlacedPowerUpItem = nullptr;
};



UCLASS()
class BOMBERMANTEST_API UGridEmptyMeshTile : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	UGridEmptyMeshTile();
	FVector2D CreateTile(UStaticMesh* MapFloorMesh, FIntPoint gridTilePoint);
	void DestroyActor();

protected:
	UStaticMesh* TileStaticMesh = nullptr;
	float tileElevation = 0.f;

private:
	float tileScale = 0.f;
	float tileSize = 0.f;
};

UCLASS()
class BOMBERMANTEST_API UGridUnbreakableBrickMeshTile : public UGridEmptyMeshTile
{
	GENERATED_BODY()

public:
	UGridUnbreakableBrickMeshTile();
};

UCLASS()
class BOMBERMANTEST_API UGridBreakableBrickMeshTile : public UGridEmptyMeshTile
{
	GENERATED_BODY()

public:
	UGridBreakableBrickMeshTile();
};

UCLASS()
class BOMBERMANTEST_API UGridStartingPointMeshTile : public UGridEmptyMeshTile
{
	GENERATED_BODY()

public:
	UGridStartingPointMeshTile();
};
