#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MainCharacter.h"
#include "BombermanTestGameModeBase.generated.h"

class AMapGrid;

enum class EPlayersIndex
{
	PLAYER_0,
	PLAYER_1,
	COUNT
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FEndGameDelegate);

UCLASS()
class BOMBERMANTEST_API ABombermanTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage) override;
	virtual void Tick(float DeltaTime) override;

public:
	UFUNCTION(BlueprintCallable, Category = "GameFlowMethods")
	void StartGame(int players);
	UFUNCTION(BlueprintCallable, Category = "GameFlowMethods")
	void ResetGame();
	UPROPERTY(BlueprintAssignable, Category = "GameFlowMethods")
	FEndGameDelegate EndGameDelegate;

	UFUNCTION(BlueprintCallable, Category = "HelperMethods")
	AMainCharacter* GetPlayerP1() { return Player_1; }
	UFUNCTION(BlueprintCallable, Category = "HelperMethods")
	AMainCharacter* GetPlayerP2() { return Player_2; }
	
	UFUNCTION(BlueprintCallable, Category = "UIHelperMethods")
	FString GetWinnerPlayerText() { return WinnerPlayer == Player_1 ? "PLAYER 1 WON" : "PLAYER 2 WON"; };
	UFUNCTION(BlueprintCallable, Category = "UIHelperMethods")
	bool IsMatchDraw() { return WinnerPlayer == nullptr; };
	UFUNCTION(BlueprintCallable, Category = "UIHelperMethods")
	int GetWinnerScore() { return WinnerPlayer == Player_1 ? Player_1->GetCharacterScore() : Player_2->GetCharacterScore(); };

	UFUNCTION()
	void OnMapGridCreated();
	UFUNCTION()
	void OnPlayerDied();

private:
	void Initialize();
	void LoadCharactersMaterial();
	void SpawnMapGrid();
	void SpawnPlayers();
	void OnPlayerDeathTimerEnds();

public:
	UPROPERTY(BlueprintReadWrite, Category = "GameFlowVariables")
	float initialMatchTime = 0.f;
	UPROPERTY(BlueprintReadWrite, Category = "GameFlowVariables")
	float matchTimeLeft = 0.f;
	UPROPERTY(BlueprintReadWrite, Category = "GameFlowVariables")
	int highScore = 0;

private:
	int numPlayers = 2;

	AMapGrid* MapGrid = nullptr;
	TArray<UMaterial*> CharactersMaterial;
	bool onMatchEnded = false;

	AMainCharacter* Player_1 = nullptr;
	AMainCharacter* Player_2 = nullptr;
	AMainCharacter* WinnerPlayer = nullptr;

	FTimerHandle PlayerDeathTimer;
	FTimerHandle MatchEndTimer;
};
