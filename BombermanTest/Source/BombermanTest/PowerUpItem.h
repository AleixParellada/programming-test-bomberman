#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EGridTileTypes.h"
#include "PowerUpItem.generated.h"

UCLASS()
class BOMBERMANTEST_API APowerUpItem : public AActor
{
	GENERATED_BODY()

public:
	APowerUpItem();
	void Initialize(EPowerUpType Type);
	void DestroyActor();
	EPowerUpType GetPowerUpType() { return PowerUpType; }

private:
	void SetPowerUpMaterial();

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "ActorMeshComponent")
	UStaticMeshComponent* StaticMeshComponent = nullptr;

private:
	EPowerUpType PowerUpType = EPowerUpType::COUNT;
};
