#include "MainCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Bomb.h"
#include "GridTile.h"
#include "MapGrid.h"
#include "EGridTileTypes.h"
#include "Components/CapsuleComponent.h" 
#include "PowerUpItem.h" 
#include "EGridTileTypes.h" 
#include "Bomb.h" 
#include "BombExplosionFire.h" 

AMainCharacter::AMainCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	movementSpeed = 100.f;
	movementSpeedToIncrease = 10.f;
	bombExplosionPower = 1;
	numBombs = 1;
	remoteBombPowerUpDuration = 10.f;
}

void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	this->GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &AMainCharacter::OnOverlapBegin);
}

void AMainCharacter::Initialize(AMapGrid* Map, AGridTile* Current)
{
	MapGrid = Map;
	CurrentGridTile = Current;
	availableBombs = numBombs;

	hasPlayerOverlappedWithFire = false;
	this->SetActorHiddenInGame(false);

	SetActorRelativeLocation(FVector(CurrentGridTile->GetTileGridRelativePosition(), this->GetActorLocation().Z));
}

void AMainCharacter::SetSkeletalMeshMaterial(UMaterial* MaterialToApply)
{
	USkeletalMeshComponent* SkeletalMeshComponent = GetMesh();
	for (int i = 1; i < 4; i++)
	{
		SkeletalMeshComponent->SetMaterial(i, MaterialToApply);
	}
}

void AMainCharacter::InjectSecondPlayer(AMainCharacter* Player)
{
	Player_2 = Player;
}

// As keyboard input is always consumed by player 0 this workaround had to be done, in which P1 has a reference to P2 to send the keyboard input events.
void AMainCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	CharacterInputComponent = PlayerInputComponent;

	CharacterInputComponent->BindAction<FDropBombDelegate>(TEXT("DropBomb_P1"), EInputEvent::IE_Pressed, this, &AMainCharacter::ReceiveDropBombInput, true);
	CharacterInputComponent->BindAction<FDropBombDelegate>(TEXT("DetonateBombs_P1"), EInputEvent::IE_Pressed, this, &AMainCharacter::ReceiveDetonateBombInput, true);
	CharacterInputComponent->BindAction<FMoveCharacterDelegate>(TEXT("MoveLeft_P1"), EInputEvent::IE_Pressed, this, &AMainCharacter::ReceiveMovementInput, FIntPoint(-1,0), true);
	CharacterInputComponent->BindAction<FMoveCharacterDelegate>(TEXT("MoveRight_P1"), EInputEvent::IE_Pressed, this, &AMainCharacter::ReceiveMovementInput, FIntPoint(1, 0), true);
	CharacterInputComponent->BindAction<FMoveCharacterDelegate>(TEXT("MoveUp_P1"), EInputEvent::IE_Pressed, this, &AMainCharacter::ReceiveMovementInput, FIntPoint(0,-1), true);
	CharacterInputComponent->BindAction<FMoveCharacterDelegate>(TEXT("MoveDown_P1"), EInputEvent::IE_Pressed, this, &AMainCharacter::ReceiveMovementInput, FIntPoint(0,1), true);

	CharacterInputComponent->BindAction<FDropBombDelegate>(TEXT("DropBomb_P2"), EInputEvent::IE_Pressed, this, &AMainCharacter::ReceiveDropBombInput, false);
	CharacterInputComponent->BindAction<FDropBombDelegate>(TEXT("DetonateBombs_P2"), EInputEvent::IE_Pressed, this, &AMainCharacter::ReceiveDetonateBombInput, false);
	CharacterInputComponent->BindAction<FMoveCharacterDelegate>(TEXT("MoveLeft_P2"), EInputEvent::IE_Pressed, this, &AMainCharacter::ReceiveMovementInput, FIntPoint(-1, 0), false);
	CharacterInputComponent->BindAction<FMoveCharacterDelegate>(TEXT("MoveRight_P2"), EInputEvent::IE_Pressed, this, &AMainCharacter::ReceiveMovementInput, FIntPoint(1, 0), false);
	CharacterInputComponent->BindAction<FMoveCharacterDelegate>(TEXT("MoveUp_P2"), EInputEvent::IE_Pressed, this, &AMainCharacter::ReceiveMovementInput, FIntPoint(0, -1), false);
	CharacterInputComponent->BindAction<FMoveCharacterDelegate>(TEXT("MoveDown_P2"), EInputEvent::IE_Pressed, this, &AMainCharacter::ReceiveMovementInput, FIntPoint(0, 1), false);
}

void AMainCharacter::ReceiveMovementInput(FIntPoint direction, bool isPlayer1)
{
	if (isPlayer1)
	{
		if (!hasPlayerOverlappedWithFire && !hasPendantMovement)
		{
			ProcessMovementInput(direction);
		}
	}
	else if(Player_2)
	{
		if (!hasPlayerOverlappedWithFire && !Player_2->GetHasPendantMovement())
		{
			Player_2->ProcessMovementInput(direction);
		}
	}
}

void AMainCharacter::ProcessMovementInput(FIntPoint direction)
{
	DesiredMovementGridTile = MapGrid->GetGridTileAtPoint(CurrentGridTile->GetTileGridPoint() + direction);
	if (DesiredMovementGridTile && DesiredMovementGridTile->IsWalkable())
	{
		hasPendantMovement = true;
		RotateCharacter(direction);
	}
}

void AMainCharacter::RotateCharacter(FIntPoint direction)
{
	FRotator actorRotation = FRotator(0.f);
	if (direction.Y == 0)
	{
		actorRotation.Yaw = direction.X == 1 ? -90.f : 90.f;
	}
	else
	{
		actorRotation.Yaw = direction.Y == 1 ? 0.f : 180.f;
	}
	
	GetMesh()->SetWorldRotation(FQuat(actorRotation));
}

void AMainCharacter::ReceiveDropBombInput(bool isPlayer1)
{
	if (isPlayer1)
	{
		if (!hasPlayerOverlappedWithFire)
		{
			DropBomb();
		}
	}
	else if(Player_2 && !Player_2->HasPlayerOverlappedWithFire())
	{
		Player_2->DropBomb();
	}
}

void AMainCharacter::DropBomb()
{
	if (availableBombs <= 0)
	{
		return;
	}

	UClass* BombBPClass = Cast<UClass>(StaticLoadObject(UClass::StaticClass(), NULL, TEXT("/Game/Blueprints/BP_Bomb.BP_Bomb_C")));
	check(BombBPClass)

	ABomb* Bomb = GetWorld()->SpawnActor<ABomb>(BombBPClass, FVector(CurrentGridTile->GetTileGridRelativePosition(), 1.f), FRotator(0.f), FActorSpawnParameters());
	check(Bomb)
	Bomb->Initialize(MapGrid, CurrentGridTile, bombExplosionPower, isRemoteBombPowerUpActive);

	CurrentGridTile->InjectBomb(Bomb);
	Bomb->BombExplosionDelegate.AddDynamic(this, &AMainCharacter::OnBombExploded);
	Bomb->BombFireOverlapPlayerDelegate.AddDynamic(this, &AMainCharacter::OnFireOverlappedActor);

	if (isRemoteBombPowerUpActive)
	{
		RemoteControlledBomb = Bomb;
	}
	availableBombs--;
}

void AMainCharacter::OnBombExploded()
{
	if (availableBombs < numBombs)
	{
		availableBombs++;
	}
	if(isRemoteBombPowerUpActive)
	{
		RemoteControlledBomb = nullptr;
	}
}

void AMainCharacter::OnRemoteBombPowerUpExpires()
{
	isRemoteBombPowerUpActive = false;
	availableBombs = numBombs;
	DetonateBombs();
}

void AMainCharacter::ReceiveDetonateBombInput(bool isPlayer1)
{
	if (isPlayer1)
	{
		if (isRemoteBombPowerUpActive && !hasPlayerOverlappedWithFire)
		{
			DetonateBombs();
		}
	}
	else if(Player_2 && Player_2->HasRemoteBombPowerUpActive())
	{
		if (!Player_2->HasPlayerOverlappedWithFire())
		{
			Player_2->DetonateBombs();
		}
	}
}

void AMainCharacter::DetonateBombs()
{
	if (RemoteControlledBomb)
	{
		RemoteControlledBomb->ExplodeBomb();
	}
	RemoteControlledBomb = nullptr;
}

void AMainCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (hasPendantMovement)
	{
		float frameIndependentSpeed = movementSpeed + (movementSpeed * DeltaTime);
		frameIndependentSpeed *= 0.1f;
		MoveCharacter(frameIndependentSpeed);
	}
}

void AMainCharacter::MoveCharacter(float speed)
{
	FVector2D actorPosition = FVector2D(this->GetActorLocation().X, this->GetActorLocation().Y);
	FVector2D desiredPosition = DesiredMovementGridTile->GetTileGridRelativePosition();
	if (FVector2D::Distance(actorPosition, desiredPosition) < 10.f)
	{
		hasPendantMovement = false;
		CurrentGridTile = DesiredMovementGridTile;
		this->SetActorLocation(FVector(desiredPosition, GetActorLocation().Z));
		return;
	}

	FVector2D direction = desiredPosition - actorPosition;
	direction.Normalize();
	FVector2D movementVector = actorPosition + direction * speed;
	this->SetActorLocation(FVector(movementVector, GetActorLocation().Z));
}

void AMainCharacter::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (hasPlayerOverlappedWithFire)
	{
		return;
	}

	APowerUpItem* PowerUpItem = Cast<APowerUpItem>(OtherActor);
	if (!PowerUpItem)
	{
		return;
	}

	EPowerUpType PowerUpType = PowerUpItem->GetPowerUpType();
	switch (PowerUpType)
	{
		case EPowerUpType::FAST_RUN_SPEED: 
		{
			if (movementSpeed <= 90.f)
			{
				movementSpeed += movementSpeedToIncrease;
			}
		}
		break;
		case EPowerUpType::LONG_BOMB_BLAST: 
		{
			bombExplosionPower++;
		}
		break;
		case EPowerUpType::MORE_BOMBS:
		{
			if (!isRemoteBombPowerUpActive)
			{
				availableBombs++;
			}
			numBombs++;
		}
		break;
		case EPowerUpType::REMOTE_CONTROLLED_BOMBS:
		{
			if (isRemoteBombPowerUpActive)
			{
				GetWorldTimerManager().ClearTimer(BombPowerUpDurationTimer);
			}

			availableBombs = 1;
			GetWorldTimerManager().SetTimer(BombPowerUpDurationTimer, this, &AMainCharacter::OnRemoteBombPowerUpExpires, remoteBombPowerUpDuration, false, remoteBombPowerUpDuration);
			isRemoteBombPowerUpActive = true;
		}
		break;
	}

	characterScore += 100;
	PowerUpItem->DestroyActor();
}

void AMainCharacter::OnFireOverlappedActor(AMainCharacter* OverlappingActor)
{
	if (!OverlappingActor->HasPlayerOverlappedWithFire())
	{
		if (OverlappingActor != this)
		{
			characterScore += 500;
		}
		OverlappingActor->HidePlayerOnFireExplosionTrigger();
		OverlappingActor->PlayerDeathDelegate.Broadcast();
	}
}

void AMainCharacter::HidePlayerOnFireExplosionTrigger()
{
	this->SetActorHiddenInGame(true);
	hasPlayerOverlappedWithFire = true;
	
	if (RemoteControlledBomb)
	{
		RemoteControlledBomb->DestroyActor();
		RemoteControlledBomb = nullptr;
	}

	GetWorldTimerManager().ClearTimer(BombPowerUpDurationTimer);
	availableBombs = 0;
}

void AMainCharacter::ResetActor()
{
	MapGrid = nullptr;
	CurrentGridTile = nullptr;
	Player_2 = nullptr;
	DesiredMovementGridTile = nullptr;
	hasPendantMovement = false;

	isRemoteBombPowerUpActive = false;
	numBombs = 1;
	bombExplosionPower = 1;
	availableBombs = numBombs;
	characterScore = 0;

	PlayerDeathDelegate.Clear();
}