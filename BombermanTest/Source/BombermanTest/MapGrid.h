#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MapGrid.generated.h"

class AGridTile;
enum class EGridTileTypes;
enum class ETileDirectionsToCheck;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FMapGridFilledDelegate);

UCLASS()
class BOMBERMANTEST_API AMapGrid : public AActor
{
	GENERATED_BODY()

public:
	AMapGrid();
	void CreateGrid();
	void ResetActor();

	AGridTile* GetAdjacentGridTileAtDirection(AGridTile* ReferenceGridTile, ETileDirectionsToCheck DirectionToCheck);
	AGridTile* GetGridTileAtPoint(FIntPoint tilePoint);
	void GetMapGridTilesList(TArray<AGridTile*>& gridTiles) { gridTiles = GridTilesList; }
	void GetStartingTilesList(TArray<AGridTile*>& gridTiles) { gridTiles = StartingGridTilesList; }

protected:
	virtual void BeginPlay() override;

private:
	void FillGridWithTiles(FIntPoint gridTilePosition);
	EGridTileTypes CalculateGridTileType(FIntPoint gridTilePosition);
	bool IsStartingGridTilePosition(FIntPoint gridTilePosition);
	bool IsGridTilePositionAdjacentToStartingPoint(FIntPoint gridTilePosition);
	bool IsGridTilePointOutsideMap(FIntPoint tilePoint);

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "ActorMeshComponent")
	UStaticMeshComponent* StaticMeshComponent = nullptr;
	
	FMapGridFilledDelegate MapGridFilledDelegate;

private:
	const int GRID_NM_DIMENSIONS = 11;
	TArray<AGridTile*> GridTilesList;
	TArray<AGridTile*> StartingGridTilesList;
};
