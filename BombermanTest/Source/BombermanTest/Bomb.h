#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Bomb.generated.h"

class AMapGrid;
class AGridTile;
class AMainCharacter;
class ABombExplosionFire;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FBombExplosionDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBombFireOverlapPlayerDelegate, AMainCharacter*, OverlappingActor);

UCLASS()
class BOMBERMANTEST_API ABomb : public AActor
{
	GENERATED_BODY()

public:
	ABomb();
	void Initialize(AMapGrid* Map, AGridTile* GridTile, int explosionPower, bool isRemotelyDetonated);
	void ExplodeBomb();
	void DestroyActor();

	UFUNCTION()
	void OnBombExplosionFireOverlapPlayer(AMainCharacter* OverlappingActor);

	bool HasBombExploded() { return hasBombExploded; }

private:
	void GetGridTilesAffected(TArray<AGridTile*>& gridTilesAffected);
	void CreateBombExplosionFireAtGridTile(AGridTile* GridTile);
	void GetBombExplosionFireRotation(AGridTile* GridTile, FRotator& desiredRotation);

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "ActorStaticMeshComponent")
	UStaticMeshComponent* StaticMeshComponent = nullptr;
	UPROPERTY(EditAnywhere, Category = "TimerProperties")
	float bombTimeToExplode = 0.f;
	UPROPERTY(EditAnywhere, Category = "TimerProperties")
	float bombExplosionDuration = 0.f;

	FBombExplosionDelegate BombExplosionDelegate;
	FBombFireOverlapPlayerDelegate BombFireOverlapPlayerDelegate;

private:
	int bombExplosionPower = 0;
	bool isBombRemotelyDetonated = false;
	bool hasBombExploded = false;

	AMapGrid* MapGrid = nullptr;
	AGridTile* CurrentGridTile = nullptr;

	UClass* BombExplosionFireBPClass = nullptr;
	TArray<ABombExplosionFire*> BombExplosionFireList;

	FTimerHandle ExplodeBombTimerHandle;
	FTimerHandle BombExplosionTimerHandle;
};
