// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BombermanTest/BombExplosionFire.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBombExplosionFire() {}
// Cross Module References
	BOMBERMANTEST_API UFunction* Z_Construct_UDelegateFunction_BombermanTest_BombExplosionFireOverlapPlayerDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_BombermanTest();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_AMainCharacter_NoRegister();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_ABombExplosionFire_NoRegister();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_ABombExplosionFire();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_BombermanTest_BombExplosionFireOverlapPlayerDelegate__DelegateSignature_Statics
	{
		struct _Script_BombermanTest_eventBombExplosionFireOverlapPlayerDelegate_Parms
		{
			AMainCharacter* OverlappingActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappingActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_BombermanTest_BombExplosionFireOverlapPlayerDelegate__DelegateSignature_Statics::NewProp_OverlappingActor = { "OverlappingActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_BombermanTest_eventBombExplosionFireOverlapPlayerDelegate_Parms, OverlappingActor), Z_Construct_UClass_AMainCharacter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_BombermanTest_BombExplosionFireOverlapPlayerDelegate__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_BombermanTest_BombExplosionFireOverlapPlayerDelegate__DelegateSignature_Statics::NewProp_OverlappingActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_BombermanTest_BombExplosionFireOverlapPlayerDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BombExplosionFire.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_BombermanTest_BombExplosionFireOverlapPlayerDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_BombermanTest, nullptr, "BombExplosionFireOverlapPlayerDelegate__DelegateSignature", nullptr, nullptr, sizeof(_Script_BombermanTest_eventBombExplosionFireOverlapPlayerDelegate_Parms), Z_Construct_UDelegateFunction_BombermanTest_BombExplosionFireOverlapPlayerDelegate__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_BombermanTest_BombExplosionFireOverlapPlayerDelegate__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_BombermanTest_BombExplosionFireOverlapPlayerDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_BombermanTest_BombExplosionFireOverlapPlayerDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_BombermanTest_BombExplosionFireOverlapPlayerDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_BombermanTest_BombExplosionFireOverlapPlayerDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(ABombExplosionFire::execOnOverlap)
	{
		P_GET_OBJECT(AActor,Z_Param_ThisActor);
		P_GET_OBJECT(AActor,Z_Param_OverlappingActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnOverlap(Z_Param_ThisActor,Z_Param_OverlappingActor);
		P_NATIVE_END;
	}
	void ABombExplosionFire::StaticRegisterNativesABombExplosionFire()
	{
		UClass* Class = ABombExplosionFire::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnOverlap", &ABombExplosionFire::execOnOverlap },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABombExplosionFire_OnOverlap_Statics
	{
		struct BombExplosionFire_eventOnOverlap_Parms
		{
			AActor* ThisActor;
			AActor* OverlappingActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverlappingActor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ThisActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABombExplosionFire_OnOverlap_Statics::NewProp_OverlappingActor = { "OverlappingActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BombExplosionFire_eventOnOverlap_Parms, OverlappingActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABombExplosionFire_OnOverlap_Statics::NewProp_ThisActor = { "ThisActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BombExplosionFire_eventOnOverlap_Parms, ThisActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABombExplosionFire_OnOverlap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABombExplosionFire_OnOverlap_Statics::NewProp_OverlappingActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABombExplosionFire_OnOverlap_Statics::NewProp_ThisActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABombExplosionFire_OnOverlap_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BombExplosionFire.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABombExplosionFire_OnOverlap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABombExplosionFire, nullptr, "OnOverlap", nullptr, nullptr, sizeof(BombExplosionFire_eventOnOverlap_Parms), Z_Construct_UFunction_ABombExplosionFire_OnOverlap_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombExplosionFire_OnOverlap_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABombExplosionFire_OnOverlap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombExplosionFire_OnOverlap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABombExplosionFire_OnOverlap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABombExplosionFire_OnOverlap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABombExplosionFire_NoRegister()
	{
		return ABombExplosionFire::StaticClass();
	}
	struct Z_Construct_UClass_ABombExplosionFire_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StaticMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_StaticMeshComponent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABombExplosionFire_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_BombermanTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABombExplosionFire_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABombExplosionFire_OnOverlap, "OnOverlap" }, // 3842868021
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABombExplosionFire_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "BombExplosionFire.h" },
		{ "ModuleRelativePath", "BombExplosionFire.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABombExplosionFire_Statics::NewProp_StaticMeshComponent_MetaData[] = {
		{ "Category", "ActorMeshComponent" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "BombExplosionFire.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ABombExplosionFire_Statics::NewProp_StaticMeshComponent = { "StaticMeshComponent", nullptr, (EPropertyFlags)0x00100000000a000d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABombExplosionFire, StaticMeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ABombExplosionFire_Statics::NewProp_StaticMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABombExplosionFire_Statics::NewProp_StaticMeshComponent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABombExplosionFire_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABombExplosionFire_Statics::NewProp_StaticMeshComponent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABombExplosionFire_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABombExplosionFire>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABombExplosionFire_Statics::ClassParams = {
		&ABombExplosionFire::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ABombExplosionFire_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ABombExplosionFire_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABombExplosionFire_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABombExplosionFire_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABombExplosionFire()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABombExplosionFire_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABombExplosionFire, 2414295050);
	template<> BOMBERMANTEST_API UClass* StaticClass<ABombExplosionFire>()
	{
		return ABombExplosionFire::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABombExplosionFire(Z_Construct_UClass_ABombExplosionFire, &ABombExplosionFire::StaticClass, TEXT("/Script/BombermanTest"), TEXT("ABombExplosionFire"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABombExplosionFire);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
