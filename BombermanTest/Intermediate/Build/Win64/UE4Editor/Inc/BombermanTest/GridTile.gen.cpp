// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BombermanTest/GridTile.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGridTile() {}
// Cross Module References
	BOMBERMANTEST_API UClass* Z_Construct_UClass_AGridTile_NoRegister();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_AGridTile();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_BombermanTest();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_UGridEmptyMeshTile_NoRegister();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_UGridEmptyMeshTile();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_UGridUnbreakableBrickMeshTile_NoRegister();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_UGridUnbreakableBrickMeshTile();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_UGridBreakableBrickMeshTile_NoRegister();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_UGridBreakableBrickMeshTile();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_UGridStartingPointMeshTile_NoRegister();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_UGridStartingPointMeshTile();
// End Cross Module References
	void AGridTile::StaticRegisterNativesAGridTile()
	{
	}
	UClass* Z_Construct_UClass_AGridTile_NoRegister()
	{
		return AGridTile::StaticClass();
	}
	struct Z_Construct_UClass_AGridTile_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AGridTile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_BombermanTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AGridTile_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "GridTile.h" },
		{ "ModuleRelativePath", "GridTile.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AGridTile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AGridTile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AGridTile_Statics::ClassParams = {
		&AGridTile::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AGridTile_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AGridTile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AGridTile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AGridTile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AGridTile, 3059857588);
	template<> BOMBERMANTEST_API UClass* StaticClass<AGridTile>()
	{
		return AGridTile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AGridTile(Z_Construct_UClass_AGridTile, &AGridTile::StaticClass, TEXT("/Script/BombermanTest"), TEXT("AGridTile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AGridTile);
	void UGridEmptyMeshTile::StaticRegisterNativesUGridEmptyMeshTile()
	{
	}
	UClass* Z_Construct_UClass_UGridEmptyMeshTile_NoRegister()
	{
		return UGridEmptyMeshTile::StaticClass();
	}
	struct Z_Construct_UClass_UGridEmptyMeshTile_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGridEmptyMeshTile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UStaticMeshComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_BombermanTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridEmptyMeshTile_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object Activation Components|Activation Trigger" },
		{ "IncludePath", "GridTile.h" },
		{ "ModuleRelativePath", "GridTile.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGridEmptyMeshTile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGridEmptyMeshTile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGridEmptyMeshTile_Statics::ClassParams = {
		&UGridEmptyMeshTile::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGridEmptyMeshTile_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGridEmptyMeshTile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGridEmptyMeshTile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGridEmptyMeshTile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGridEmptyMeshTile, 1465611535);
	template<> BOMBERMANTEST_API UClass* StaticClass<UGridEmptyMeshTile>()
	{
		return UGridEmptyMeshTile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGridEmptyMeshTile(Z_Construct_UClass_UGridEmptyMeshTile, &UGridEmptyMeshTile::StaticClass, TEXT("/Script/BombermanTest"), TEXT("UGridEmptyMeshTile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGridEmptyMeshTile);
	void UGridUnbreakableBrickMeshTile::StaticRegisterNativesUGridUnbreakableBrickMeshTile()
	{
	}
	UClass* Z_Construct_UClass_UGridUnbreakableBrickMeshTile_NoRegister()
	{
		return UGridUnbreakableBrickMeshTile::StaticClass();
	}
	struct Z_Construct_UClass_UGridUnbreakableBrickMeshTile_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGridUnbreakableBrickMeshTile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGridEmptyMeshTile,
		(UObject* (*)())Z_Construct_UPackage__Script_BombermanTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridUnbreakableBrickMeshTile_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object Activation Components|Activation Trigger" },
		{ "IncludePath", "GridTile.h" },
		{ "ModuleRelativePath", "GridTile.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGridUnbreakableBrickMeshTile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGridUnbreakableBrickMeshTile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGridUnbreakableBrickMeshTile_Statics::ClassParams = {
		&UGridUnbreakableBrickMeshTile::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGridUnbreakableBrickMeshTile_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGridUnbreakableBrickMeshTile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGridUnbreakableBrickMeshTile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGridUnbreakableBrickMeshTile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGridUnbreakableBrickMeshTile, 2965752921);
	template<> BOMBERMANTEST_API UClass* StaticClass<UGridUnbreakableBrickMeshTile>()
	{
		return UGridUnbreakableBrickMeshTile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGridUnbreakableBrickMeshTile(Z_Construct_UClass_UGridUnbreakableBrickMeshTile, &UGridUnbreakableBrickMeshTile::StaticClass, TEXT("/Script/BombermanTest"), TEXT("UGridUnbreakableBrickMeshTile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGridUnbreakableBrickMeshTile);
	void UGridBreakableBrickMeshTile::StaticRegisterNativesUGridBreakableBrickMeshTile()
	{
	}
	UClass* Z_Construct_UClass_UGridBreakableBrickMeshTile_NoRegister()
	{
		return UGridBreakableBrickMeshTile::StaticClass();
	}
	struct Z_Construct_UClass_UGridBreakableBrickMeshTile_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGridBreakableBrickMeshTile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGridEmptyMeshTile,
		(UObject* (*)())Z_Construct_UPackage__Script_BombermanTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridBreakableBrickMeshTile_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object Activation Components|Activation Trigger" },
		{ "IncludePath", "GridTile.h" },
		{ "ModuleRelativePath", "GridTile.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGridBreakableBrickMeshTile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGridBreakableBrickMeshTile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGridBreakableBrickMeshTile_Statics::ClassParams = {
		&UGridBreakableBrickMeshTile::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGridBreakableBrickMeshTile_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGridBreakableBrickMeshTile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGridBreakableBrickMeshTile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGridBreakableBrickMeshTile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGridBreakableBrickMeshTile, 3043550274);
	template<> BOMBERMANTEST_API UClass* StaticClass<UGridBreakableBrickMeshTile>()
	{
		return UGridBreakableBrickMeshTile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGridBreakableBrickMeshTile(Z_Construct_UClass_UGridBreakableBrickMeshTile, &UGridBreakableBrickMeshTile::StaticClass, TEXT("/Script/BombermanTest"), TEXT("UGridBreakableBrickMeshTile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGridBreakableBrickMeshTile);
	void UGridStartingPointMeshTile::StaticRegisterNativesUGridStartingPointMeshTile()
	{
	}
	UClass* Z_Construct_UClass_UGridStartingPointMeshTile_NoRegister()
	{
		return UGridStartingPointMeshTile::StaticClass();
	}
	struct Z_Construct_UClass_UGridStartingPointMeshTile_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGridStartingPointMeshTile_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGridEmptyMeshTile,
		(UObject* (*)())Z_Construct_UPackage__Script_BombermanTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGridStartingPointMeshTile_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Object Activation Components|Activation Trigger" },
		{ "IncludePath", "GridTile.h" },
		{ "ModuleRelativePath", "GridTile.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGridStartingPointMeshTile_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGridStartingPointMeshTile>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGridStartingPointMeshTile_Statics::ClassParams = {
		&UGridStartingPointMeshTile::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B010A4u,
		METADATA_PARAMS(Z_Construct_UClass_UGridStartingPointMeshTile_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGridStartingPointMeshTile_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGridStartingPointMeshTile()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGridStartingPointMeshTile_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGridStartingPointMeshTile, 2371702270);
	template<> BOMBERMANTEST_API UClass* StaticClass<UGridStartingPointMeshTile>()
	{
		return UGridStartingPointMeshTile::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGridStartingPointMeshTile(Z_Construct_UClass_UGridStartingPointMeshTile, &UGridStartingPointMeshTile::StaticClass, TEXT("/Script/BombermanTest"), TEXT("UGridStartingPointMeshTile"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGridStartingPointMeshTile);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
