// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AMainCharacter;
#ifdef BOMBERMANTEST_BombermanTestGameModeBase_generated_h
#error "BombermanTestGameModeBase.generated.h already included, missing '#pragma once' in BombermanTestGameModeBase.h"
#endif
#define BOMBERMANTEST_BombermanTestGameModeBase_generated_h

#define BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_17_DELEGATE \
static inline void FEndGameDelegate_DelegateWrapper(const FMulticastScriptDelegate& EndGameDelegate) \
{ \
	EndGameDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_SPARSE_DATA
#define BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPlayerDied); \
	DECLARE_FUNCTION(execOnMapGridCreated); \
	DECLARE_FUNCTION(execGetWinnerScore); \
	DECLARE_FUNCTION(execIsMatchDraw); \
	DECLARE_FUNCTION(execGetWinnerPlayerText); \
	DECLARE_FUNCTION(execGetPlayerP2); \
	DECLARE_FUNCTION(execGetPlayerP1); \
	DECLARE_FUNCTION(execResetGame); \
	DECLARE_FUNCTION(execStartGame);


#define BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPlayerDied); \
	DECLARE_FUNCTION(execOnMapGridCreated); \
	DECLARE_FUNCTION(execGetWinnerScore); \
	DECLARE_FUNCTION(execIsMatchDraw); \
	DECLARE_FUNCTION(execGetWinnerPlayerText); \
	DECLARE_FUNCTION(execGetPlayerP2); \
	DECLARE_FUNCTION(execGetPlayerP1); \
	DECLARE_FUNCTION(execResetGame); \
	DECLARE_FUNCTION(execStartGame);


#define BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABombermanTestGameModeBase(); \
	friend struct Z_Construct_UClass_ABombermanTestGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ABombermanTestGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(ABombermanTestGameModeBase)


#define BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_INCLASS \
private: \
	static void StaticRegisterNativesABombermanTestGameModeBase(); \
	friend struct Z_Construct_UClass_ABombermanTestGameModeBase_Statics; \
public: \
	DECLARE_CLASS(ABombermanTestGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(ABombermanTestGameModeBase)


#define BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABombermanTestGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABombermanTestGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABombermanTestGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABombermanTestGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABombermanTestGameModeBase(ABombermanTestGameModeBase&&); \
	NO_API ABombermanTestGameModeBase(const ABombermanTestGameModeBase&); \
public:


#define BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABombermanTestGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABombermanTestGameModeBase(ABombermanTestGameModeBase&&); \
	NO_API ABombermanTestGameModeBase(const ABombermanTestGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABombermanTestGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABombermanTestGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABombermanTestGameModeBase)


#define BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_PRIVATE_PROPERTY_OFFSET
#define BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_19_PROLOG
#define BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_RPC_WRAPPERS \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_INCLASS \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_INCLASS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOMBERMANTEST_API UClass* StaticClass<class ABombermanTestGameModeBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BombermanTest_Source_BombermanTest_BombermanTestGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
