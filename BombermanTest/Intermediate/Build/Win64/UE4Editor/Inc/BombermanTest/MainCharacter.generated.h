// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AMainCharacter;
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef BOMBERMANTEST_MainCharacter_generated_h
#error "MainCharacter.generated.h already included, missing '#pragma once' in MainCharacter.h"
#endif
#define BOMBERMANTEST_MainCharacter_generated_h

#define BombermanTest_Source_BombermanTest_MainCharacter_h_12_DELEGATE \
static inline void FPlayerDeathDelegate_DelegateWrapper(const FMulticastScriptDelegate& PlayerDeathDelegate) \
{ \
	PlayerDeathDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define BombermanTest_Source_BombermanTest_MainCharacter_h_17_SPARSE_DATA
#define BombermanTest_Source_BombermanTest_MainCharacter_h_17_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnFireOverlappedActor); \
	DECLARE_FUNCTION(execOnBombExploded); \
	DECLARE_FUNCTION(execOnOverlapBegin);


#define BombermanTest_Source_BombermanTest_MainCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnFireOverlappedActor); \
	DECLARE_FUNCTION(execOnBombExploded); \
	DECLARE_FUNCTION(execOnOverlapBegin);


#define BombermanTest_Source_BombermanTest_MainCharacter_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMainCharacter(); \
	friend struct Z_Construct_UClass_AMainCharacter_Statics; \
public: \
	DECLARE_CLASS(AMainCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(AMainCharacter)


#define BombermanTest_Source_BombermanTest_MainCharacter_h_17_INCLASS \
private: \
	static void StaticRegisterNativesAMainCharacter(); \
	friend struct Z_Construct_UClass_AMainCharacter_Statics; \
public: \
	DECLARE_CLASS(AMainCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(AMainCharacter)


#define BombermanTest_Source_BombermanTest_MainCharacter_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMainCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainCharacter(AMainCharacter&&); \
	NO_API AMainCharacter(const AMainCharacter&); \
public:


#define BombermanTest_Source_BombermanTest_MainCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainCharacter(AMainCharacter&&); \
	NO_API AMainCharacter(const AMainCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMainCharacter)


#define BombermanTest_Source_BombermanTest_MainCharacter_h_17_PRIVATE_PROPERTY_OFFSET
#define BombermanTest_Source_BombermanTest_MainCharacter_h_14_PROLOG
#define BombermanTest_Source_BombermanTest_MainCharacter_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_MainCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_MainCharacter_h_17_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_MainCharacter_h_17_RPC_WRAPPERS \
	BombermanTest_Source_BombermanTest_MainCharacter_h_17_INCLASS \
	BombermanTest_Source_BombermanTest_MainCharacter_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BombermanTest_Source_BombermanTest_MainCharacter_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_MainCharacter_h_17_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_MainCharacter_h_17_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_MainCharacter_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_MainCharacter_h_17_INCLASS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_MainCharacter_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOMBERMANTEST_API UClass* StaticClass<class AMainCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BombermanTest_Source_BombermanTest_MainCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
