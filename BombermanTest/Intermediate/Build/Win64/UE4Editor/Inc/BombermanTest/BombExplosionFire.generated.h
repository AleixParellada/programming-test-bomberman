// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AMainCharacter;
class AActor;
#ifdef BOMBERMANTEST_BombExplosionFire_generated_h
#error "BombExplosionFire.generated.h already included, missing '#pragma once' in BombExplosionFire.h"
#endif
#define BOMBERMANTEST_BombExplosionFire_generated_h

#define BombermanTest_Source_BombermanTest_BombExplosionFire_h_9_DELEGATE \
struct _Script_BombermanTest_eventBombExplosionFireOverlapPlayerDelegate_Parms \
{ \
	AMainCharacter* OverlappingActor; \
}; \
static inline void FBombExplosionFireOverlapPlayerDelegate_DelegateWrapper(const FMulticastScriptDelegate& BombExplosionFireOverlapPlayerDelegate, AMainCharacter* OverlappingActor) \
{ \
	_Script_BombermanTest_eventBombExplosionFireOverlapPlayerDelegate_Parms Parms; \
	Parms.OverlappingActor=OverlappingActor; \
	BombExplosionFireOverlapPlayerDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_SPARSE_DATA
#define BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnOverlap);


#define BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnOverlap);


#define BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABombExplosionFire(); \
	friend struct Z_Construct_UClass_ABombExplosionFire_Statics; \
public: \
	DECLARE_CLASS(ABombExplosionFire, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(ABombExplosionFire)


#define BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_INCLASS \
private: \
	static void StaticRegisterNativesABombExplosionFire(); \
	friend struct Z_Construct_UClass_ABombExplosionFire_Statics; \
public: \
	DECLARE_CLASS(ABombExplosionFire, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(ABombExplosionFire)


#define BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABombExplosionFire(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABombExplosionFire) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABombExplosionFire); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABombExplosionFire); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABombExplosionFire(ABombExplosionFire&&); \
	NO_API ABombExplosionFire(const ABombExplosionFire&); \
public:


#define BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABombExplosionFire(ABombExplosionFire&&); \
	NO_API ABombExplosionFire(const ABombExplosionFire&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABombExplosionFire); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABombExplosionFire); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABombExplosionFire)


#define BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_PRIVATE_PROPERTY_OFFSET
#define BombermanTest_Source_BombermanTest_BombExplosionFire_h_11_PROLOG
#define BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_RPC_WRAPPERS \
	BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_INCLASS \
	BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_INCLASS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_BombExplosionFire_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOMBERMANTEST_API UClass* StaticClass<class ABombExplosionFire>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BombermanTest_Source_BombermanTest_BombExplosionFire_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
