// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOMBERMANTEST_BombermanTestGameModeStateBase_generated_h
#error "BombermanTestGameModeStateBase.generated.h already included, missing '#pragma once' in BombermanTestGameModeStateBase.h"
#endif
#define BOMBERMANTEST_BombermanTestGameModeStateBase_generated_h

#define BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_SPARSE_DATA
#define BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_RPC_WRAPPERS
#define BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_RPC_WRAPPERS_NO_PURE_DECLS
#define BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABombermanTestGameModeStateBase(); \
	friend struct Z_Construct_UClass_ABombermanTestGameModeStateBase_Statics; \
public: \
	DECLARE_CLASS(ABombermanTestGameModeStateBase, AGameStateBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(ABombermanTestGameModeStateBase)


#define BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_INCLASS \
private: \
	static void StaticRegisterNativesABombermanTestGameModeStateBase(); \
	friend struct Z_Construct_UClass_ABombermanTestGameModeStateBase_Statics; \
public: \
	DECLARE_CLASS(ABombermanTestGameModeStateBase, AGameStateBase, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(ABombermanTestGameModeStateBase)


#define BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABombermanTestGameModeStateBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABombermanTestGameModeStateBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABombermanTestGameModeStateBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABombermanTestGameModeStateBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABombermanTestGameModeStateBase(ABombermanTestGameModeStateBase&&); \
	NO_API ABombermanTestGameModeStateBase(const ABombermanTestGameModeStateBase&); \
public:


#define BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABombermanTestGameModeStateBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABombermanTestGameModeStateBase(ABombermanTestGameModeStateBase&&); \
	NO_API ABombermanTestGameModeStateBase(const ABombermanTestGameModeStateBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABombermanTestGameModeStateBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABombermanTestGameModeStateBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABombermanTestGameModeStateBase)


#define BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_PRIVATE_PROPERTY_OFFSET
#define BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_7_PROLOG
#define BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_RPC_WRAPPERS \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_INCLASS \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_INCLASS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h_10_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOMBERMANTEST_API UClass* StaticClass<class ABombermanTestGameModeStateBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BombermanTest_Source_BombermanTest_BombermanTestGameModeStateBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
