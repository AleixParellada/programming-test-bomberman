// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BombermanTest/BombermanTestGameModeStateBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBombermanTestGameModeStateBase() {}
// Cross Module References
	BOMBERMANTEST_API UClass* Z_Construct_UClass_ABombermanTestGameModeStateBase_NoRegister();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_ABombermanTestGameModeStateBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameStateBase();
	UPackage* Z_Construct_UPackage__Script_BombermanTest();
// End Cross Module References
	void ABombermanTestGameModeStateBase::StaticRegisterNativesABombermanTestGameModeStateBase()
	{
	}
	UClass* Z_Construct_UClass_ABombermanTestGameModeStateBase_NoRegister()
	{
		return ABombermanTestGameModeStateBase::StaticClass();
	}
	struct Z_Construct_UClass_ABombermanTestGameModeStateBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABombermanTestGameModeStateBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameStateBase,
		(UObject* (*)())Z_Construct_UPackage__Script_BombermanTest,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABombermanTestGameModeStateBase_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "BombermanTestGameModeStateBase.h" },
		{ "ModuleRelativePath", "BombermanTestGameModeStateBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABombermanTestGameModeStateBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABombermanTestGameModeStateBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABombermanTestGameModeStateBase_Statics::ClassParams = {
		&ABombermanTestGameModeStateBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009002A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABombermanTestGameModeStateBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABombermanTestGameModeStateBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABombermanTestGameModeStateBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABombermanTestGameModeStateBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABombermanTestGameModeStateBase, 1580603304);
	template<> BOMBERMANTEST_API UClass* StaticClass<ABombermanTestGameModeStateBase>()
	{
		return ABombermanTestGameModeStateBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABombermanTestGameModeStateBase(Z_Construct_UClass_ABombermanTestGameModeStateBase, &ABombermanTestGameModeStateBase::StaticClass, TEXT("/Script/BombermanTest"), TEXT("ABombermanTestGameModeStateBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABombermanTestGameModeStateBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
