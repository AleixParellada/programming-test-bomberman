// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOMBERMANTEST_MapGrid_generated_h
#error "MapGrid.generated.h already included, missing '#pragma once' in MapGrid.h"
#endif
#define BOMBERMANTEST_MapGrid_generated_h

#define BombermanTest_Source_BombermanTest_MapGrid_h_11_DELEGATE \
static inline void FMapGridFilledDelegate_DelegateWrapper(const FMulticastScriptDelegate& MapGridFilledDelegate) \
{ \
	MapGridFilledDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define BombermanTest_Source_BombermanTest_MapGrid_h_16_SPARSE_DATA
#define BombermanTest_Source_BombermanTest_MapGrid_h_16_RPC_WRAPPERS
#define BombermanTest_Source_BombermanTest_MapGrid_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define BombermanTest_Source_BombermanTest_MapGrid_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMapGrid(); \
	friend struct Z_Construct_UClass_AMapGrid_Statics; \
public: \
	DECLARE_CLASS(AMapGrid, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(AMapGrid)


#define BombermanTest_Source_BombermanTest_MapGrid_h_16_INCLASS \
private: \
	static void StaticRegisterNativesAMapGrid(); \
	friend struct Z_Construct_UClass_AMapGrid_Statics; \
public: \
	DECLARE_CLASS(AMapGrid, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(AMapGrid)


#define BombermanTest_Source_BombermanTest_MapGrid_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMapGrid(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMapGrid) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMapGrid); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMapGrid); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMapGrid(AMapGrid&&); \
	NO_API AMapGrid(const AMapGrid&); \
public:


#define BombermanTest_Source_BombermanTest_MapGrid_h_16_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMapGrid(AMapGrid&&); \
	NO_API AMapGrid(const AMapGrid&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMapGrid); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMapGrid); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMapGrid)


#define BombermanTest_Source_BombermanTest_MapGrid_h_16_PRIVATE_PROPERTY_OFFSET
#define BombermanTest_Source_BombermanTest_MapGrid_h_13_PROLOG
#define BombermanTest_Source_BombermanTest_MapGrid_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_MapGrid_h_16_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_MapGrid_h_16_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_MapGrid_h_16_RPC_WRAPPERS \
	BombermanTest_Source_BombermanTest_MapGrid_h_16_INCLASS \
	BombermanTest_Source_BombermanTest_MapGrid_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BombermanTest_Source_BombermanTest_MapGrid_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_MapGrid_h_16_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_MapGrid_h_16_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_MapGrid_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_MapGrid_h_16_INCLASS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_MapGrid_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOMBERMANTEST_API UClass* StaticClass<class AMapGrid>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BombermanTest_Source_BombermanTest_MapGrid_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
