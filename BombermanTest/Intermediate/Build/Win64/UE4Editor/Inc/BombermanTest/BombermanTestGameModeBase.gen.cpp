// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "BombermanTest/BombermanTestGameModeBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBombermanTestGameModeBase() {}
// Cross Module References
	BOMBERMANTEST_API UFunction* Z_Construct_UDelegateFunction_BombermanTest_EndGameDelegate__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_BombermanTest();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_ABombermanTestGameModeBase_NoRegister();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_ABombermanTestGameModeBase();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	BOMBERMANTEST_API UClass* Z_Construct_UClass_AMainCharacter_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_BombermanTest_EndGameDelegate__DelegateSignature_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_BombermanTest_EndGameDelegate__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_BombermanTest_EndGameDelegate__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_BombermanTest, nullptr, "EndGameDelegate__DelegateSignature", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_BombermanTest_EndGameDelegate__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_BombermanTest_EndGameDelegate__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_BombermanTest_EndGameDelegate__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_BombermanTest_EndGameDelegate__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(ABombermanTestGameModeBase::execOnPlayerDied)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPlayerDied();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABombermanTestGameModeBase::execOnMapGridCreated)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnMapGridCreated();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABombermanTestGameModeBase::execGetWinnerScore)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetWinnerScore();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABombermanTestGameModeBase::execIsMatchDraw)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsMatchDraw();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABombermanTestGameModeBase::execGetWinnerPlayerText)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetWinnerPlayerText();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABombermanTestGameModeBase::execGetPlayerP2)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AMainCharacter**)Z_Param__Result=P_THIS->GetPlayerP2();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABombermanTestGameModeBase::execGetPlayerP1)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AMainCharacter**)Z_Param__Result=P_THIS->GetPlayerP1();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABombermanTestGameModeBase::execResetGame)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ResetGame();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ABombermanTestGameModeBase::execStartGame)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_players);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->StartGame(Z_Param_players);
		P_NATIVE_END;
	}
	void ABombermanTestGameModeBase::StaticRegisterNativesABombermanTestGameModeBase()
	{
		UClass* Class = ABombermanTestGameModeBase::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetPlayerP1", &ABombermanTestGameModeBase::execGetPlayerP1 },
			{ "GetPlayerP2", &ABombermanTestGameModeBase::execGetPlayerP2 },
			{ "GetWinnerPlayerText", &ABombermanTestGameModeBase::execGetWinnerPlayerText },
			{ "GetWinnerScore", &ABombermanTestGameModeBase::execGetWinnerScore },
			{ "IsMatchDraw", &ABombermanTestGameModeBase::execIsMatchDraw },
			{ "OnMapGridCreated", &ABombermanTestGameModeBase::execOnMapGridCreated },
			{ "OnPlayerDied", &ABombermanTestGameModeBase::execOnPlayerDied },
			{ "ResetGame", &ABombermanTestGameModeBase::execResetGame },
			{ "StartGame", &ABombermanTestGameModeBase::execStartGame },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP1_Statics
	{
		struct BombermanTestGameModeBase_eventGetPlayerP1_Parms
		{
			AMainCharacter* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP1_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BombermanTestGameModeBase_eventGetPlayerP1_Parms, ReturnValue), Z_Construct_UClass_AMainCharacter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP1_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP1_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP1_Statics::Function_MetaDataParams[] = {
		{ "Category", "HelperMethods" },
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP1_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABombermanTestGameModeBase, nullptr, "GetPlayerP1", nullptr, nullptr, sizeof(BombermanTestGameModeBase_eventGetPlayerP1_Parms), Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP1_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP1_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP1_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP1_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP1()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP1_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP2_Statics
	{
		struct BombermanTestGameModeBase_eventGetPlayerP2_Parms
		{
			AMainCharacter* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP2_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BombermanTestGameModeBase_eventGetPlayerP2_Parms, ReturnValue), Z_Construct_UClass_AMainCharacter_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP2_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP2_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP2_Statics::Function_MetaDataParams[] = {
		{ "Category", "HelperMethods" },
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP2_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABombermanTestGameModeBase, nullptr, "GetPlayerP2", nullptr, nullptr, sizeof(BombermanTestGameModeBase_eventGetPlayerP2_Parms), Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP2_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP2_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP2_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP2_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP2()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP2_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerPlayerText_Statics
	{
		struct BombermanTestGameModeBase_eventGetWinnerPlayerText_Parms
		{
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerPlayerText_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BombermanTestGameModeBase_eventGetWinnerPlayerText_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerPlayerText_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerPlayerText_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerPlayerText_Statics::Function_MetaDataParams[] = {
		{ "Category", "UIHelperMethods" },
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerPlayerText_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABombermanTestGameModeBase, nullptr, "GetWinnerPlayerText", nullptr, nullptr, sizeof(BombermanTestGameModeBase_eventGetWinnerPlayerText_Parms), Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerPlayerText_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerPlayerText_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerPlayerText_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerPlayerText_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerPlayerText()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerPlayerText_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerScore_Statics
	{
		struct BombermanTestGameModeBase_eventGetWinnerScore_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerScore_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BombermanTestGameModeBase_eventGetWinnerScore_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerScore_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerScore_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerScore_Statics::Function_MetaDataParams[] = {
		{ "Category", "UIHelperMethods" },
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerScore_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABombermanTestGameModeBase, nullptr, "GetWinnerScore", nullptr, nullptr, sizeof(BombermanTestGameModeBase_eventGetWinnerScore_Parms), Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerScore_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerScore_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerScore_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerScore_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerScore()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerScore_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw_Statics
	{
		struct BombermanTestGameModeBase_eventIsMatchDraw_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((BombermanTestGameModeBase_eventIsMatchDraw_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(BombermanTestGameModeBase_eventIsMatchDraw_Parms), &Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw_Statics::Function_MetaDataParams[] = {
		{ "Category", "UIHelperMethods" },
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABombermanTestGameModeBase, nullptr, "IsMatchDraw", nullptr, nullptr, sizeof(BombermanTestGameModeBase_eventIsMatchDraw_Parms), Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABombermanTestGameModeBase_OnMapGridCreated_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABombermanTestGameModeBase_OnMapGridCreated_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABombermanTestGameModeBase_OnMapGridCreated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABombermanTestGameModeBase, nullptr, "OnMapGridCreated", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABombermanTestGameModeBase_OnMapGridCreated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_OnMapGridCreated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABombermanTestGameModeBase_OnMapGridCreated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABombermanTestGameModeBase_OnMapGridCreated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABombermanTestGameModeBase_OnPlayerDied_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABombermanTestGameModeBase_OnPlayerDied_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABombermanTestGameModeBase_OnPlayerDied_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABombermanTestGameModeBase, nullptr, "OnPlayerDied", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABombermanTestGameModeBase_OnPlayerDied_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_OnPlayerDied_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABombermanTestGameModeBase_OnPlayerDied()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABombermanTestGameModeBase_OnPlayerDied_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABombermanTestGameModeBase_ResetGame_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABombermanTestGameModeBase_ResetGame_Statics::Function_MetaDataParams[] = {
		{ "Category", "GameFlowMethods" },
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABombermanTestGameModeBase_ResetGame_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABombermanTestGameModeBase, nullptr, "ResetGame", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABombermanTestGameModeBase_ResetGame_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_ResetGame_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABombermanTestGameModeBase_ResetGame()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABombermanTestGameModeBase_ResetGame_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ABombermanTestGameModeBase_StartGame_Statics
	{
		struct BombermanTestGameModeBase_eventStartGame_Parms
		{
			int32 players;
		};
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_players;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UFunction_ABombermanTestGameModeBase_StartGame_Statics::NewProp_players = { "players", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(BombermanTestGameModeBase_eventStartGame_Parms, players), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ABombermanTestGameModeBase_StartGame_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ABombermanTestGameModeBase_StartGame_Statics::NewProp_players,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ABombermanTestGameModeBase_StartGame_Statics::Function_MetaDataParams[] = {
		{ "Category", "GameFlowMethods" },
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ABombermanTestGameModeBase_StartGame_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ABombermanTestGameModeBase, nullptr, "StartGame", nullptr, nullptr, sizeof(BombermanTestGameModeBase_eventStartGame_Parms), Z_Construct_UFunction_ABombermanTestGameModeBase_StartGame_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_StartGame_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ABombermanTestGameModeBase_StartGame_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ABombermanTestGameModeBase_StartGame_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ABombermanTestGameModeBase_StartGame()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ABombermanTestGameModeBase_StartGame_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ABombermanTestGameModeBase_NoRegister()
	{
		return ABombermanTestGameModeBase::StaticClass();
	}
	struct Z_Construct_UClass_ABombermanTestGameModeBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_highScore_MetaData[];
#endif
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_highScore;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_matchTimeLeft_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_matchTimeLeft;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_initialMatchTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_initialMatchTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndGameDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_EndGameDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABombermanTestGameModeBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_BombermanTest,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ABombermanTestGameModeBase_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP1, "GetPlayerP1" }, // 2077413474
		{ &Z_Construct_UFunction_ABombermanTestGameModeBase_GetPlayerP2, "GetPlayerP2" }, // 857132212
		{ &Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerPlayerText, "GetWinnerPlayerText" }, // 1213912181
		{ &Z_Construct_UFunction_ABombermanTestGameModeBase_GetWinnerScore, "GetWinnerScore" }, // 1199636682
		{ &Z_Construct_UFunction_ABombermanTestGameModeBase_IsMatchDraw, "IsMatchDraw" }, // 2934669486
		{ &Z_Construct_UFunction_ABombermanTestGameModeBase_OnMapGridCreated, "OnMapGridCreated" }, // 171662876
		{ &Z_Construct_UFunction_ABombermanTestGameModeBase_OnPlayerDied, "OnPlayerDied" }, // 2482148206
		{ &Z_Construct_UFunction_ABombermanTestGameModeBase_ResetGame, "ResetGame" }, // 2414517682
		{ &Z_Construct_UFunction_ABombermanTestGameModeBase_StartGame, "StartGame" }, // 1558130533
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABombermanTestGameModeBase_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "BombermanTestGameModeBase.h" },
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_highScore_MetaData[] = {
		{ "Category", "GameFlowVariables" },
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_highScore = { "highScore", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABombermanTestGameModeBase, highScore), METADATA_PARAMS(Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_highScore_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_highScore_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_matchTimeLeft_MetaData[] = {
		{ "Category", "GameFlowVariables" },
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_matchTimeLeft = { "matchTimeLeft", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABombermanTestGameModeBase, matchTimeLeft), METADATA_PARAMS(Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_matchTimeLeft_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_matchTimeLeft_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_initialMatchTime_MetaData[] = {
		{ "Category", "GameFlowVariables" },
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_initialMatchTime = { "initialMatchTime", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABombermanTestGameModeBase, initialMatchTime), METADATA_PARAMS(Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_initialMatchTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_initialMatchTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_EndGameDelegate_MetaData[] = {
		{ "Category", "GameFlowMethods" },
		{ "ModuleRelativePath", "BombermanTestGameModeBase.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_EndGameDelegate = { "EndGameDelegate", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABombermanTestGameModeBase, EndGameDelegate), Z_Construct_UDelegateFunction_BombermanTest_EndGameDelegate__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_EndGameDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_EndGameDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABombermanTestGameModeBase_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_highScore,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_matchTimeLeft,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_initialMatchTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABombermanTestGameModeBase_Statics::NewProp_EndGameDelegate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABombermanTestGameModeBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABombermanTestGameModeBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABombermanTestGameModeBase_Statics::ClassParams = {
		&ABombermanTestGameModeBase::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ABombermanTestGameModeBase_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ABombermanTestGameModeBase_Statics::PropPointers),
		0,
		0x009002ACu,
		METADATA_PARAMS(Z_Construct_UClass_ABombermanTestGameModeBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABombermanTestGameModeBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABombermanTestGameModeBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABombermanTestGameModeBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABombermanTestGameModeBase, 2172058022);
	template<> BOMBERMANTEST_API UClass* StaticClass<ABombermanTestGameModeBase>()
	{
		return ABombermanTestGameModeBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABombermanTestGameModeBase(Z_Construct_UClass_ABombermanTestGameModeBase, &ABombermanTestGameModeBase::StaticClass, TEXT("/Script/BombermanTest"), TEXT("ABombermanTestGameModeBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABombermanTestGameModeBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
