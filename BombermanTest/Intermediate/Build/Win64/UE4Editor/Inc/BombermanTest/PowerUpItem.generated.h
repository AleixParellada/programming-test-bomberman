// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOMBERMANTEST_PowerUpItem_generated_h
#error "PowerUpItem.generated.h already included, missing '#pragma once' in PowerUpItem.h"
#endif
#define BOMBERMANTEST_PowerUpItem_generated_h

#define BombermanTest_Source_BombermanTest_PowerUpItem_h_11_SPARSE_DATA
#define BombermanTest_Source_BombermanTest_PowerUpItem_h_11_RPC_WRAPPERS
#define BombermanTest_Source_BombermanTest_PowerUpItem_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define BombermanTest_Source_BombermanTest_PowerUpItem_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPowerUpItem(); \
	friend struct Z_Construct_UClass_APowerUpItem_Statics; \
public: \
	DECLARE_CLASS(APowerUpItem, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(APowerUpItem)


#define BombermanTest_Source_BombermanTest_PowerUpItem_h_11_INCLASS \
private: \
	static void StaticRegisterNativesAPowerUpItem(); \
	friend struct Z_Construct_UClass_APowerUpItem_Statics; \
public: \
	DECLARE_CLASS(APowerUpItem, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(APowerUpItem)


#define BombermanTest_Source_BombermanTest_PowerUpItem_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APowerUpItem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APowerUpItem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APowerUpItem); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APowerUpItem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APowerUpItem(APowerUpItem&&); \
	NO_API APowerUpItem(const APowerUpItem&); \
public:


#define BombermanTest_Source_BombermanTest_PowerUpItem_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APowerUpItem(APowerUpItem&&); \
	NO_API APowerUpItem(const APowerUpItem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APowerUpItem); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APowerUpItem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APowerUpItem)


#define BombermanTest_Source_BombermanTest_PowerUpItem_h_11_PRIVATE_PROPERTY_OFFSET
#define BombermanTest_Source_BombermanTest_PowerUpItem_h_8_PROLOG
#define BombermanTest_Source_BombermanTest_PowerUpItem_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_PowerUpItem_h_11_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_PowerUpItem_h_11_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_PowerUpItem_h_11_RPC_WRAPPERS \
	BombermanTest_Source_BombermanTest_PowerUpItem_h_11_INCLASS \
	BombermanTest_Source_BombermanTest_PowerUpItem_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BombermanTest_Source_BombermanTest_PowerUpItem_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_PowerUpItem_h_11_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_PowerUpItem_h_11_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_PowerUpItem_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_PowerUpItem_h_11_INCLASS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_PowerUpItem_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOMBERMANTEST_API UClass* StaticClass<class APowerUpItem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BombermanTest_Source_BombermanTest_PowerUpItem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
