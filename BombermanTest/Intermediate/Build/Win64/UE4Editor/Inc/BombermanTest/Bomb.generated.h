// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AMainCharacter;
#ifdef BOMBERMANTEST_Bomb_generated_h
#error "Bomb.generated.h already included, missing '#pragma once' in Bomb.h"
#endif
#define BOMBERMANTEST_Bomb_generated_h

#define BombermanTest_Source_BombermanTest_Bomb_h_13_DELEGATE \
struct _Script_BombermanTest_eventBombFireOverlapPlayerDelegate_Parms \
{ \
	AMainCharacter* OverlappingActor; \
}; \
static inline void FBombFireOverlapPlayerDelegate_DelegateWrapper(const FMulticastScriptDelegate& BombFireOverlapPlayerDelegate, AMainCharacter* OverlappingActor) \
{ \
	_Script_BombermanTest_eventBombFireOverlapPlayerDelegate_Parms Parms; \
	Parms.OverlappingActor=OverlappingActor; \
	BombFireOverlapPlayerDelegate.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define BombermanTest_Source_BombermanTest_Bomb_h_12_DELEGATE \
static inline void FBombExplosionDelegate_DelegateWrapper(const FMulticastScriptDelegate& BombExplosionDelegate) \
{ \
	BombExplosionDelegate.ProcessMulticastDelegate<UObject>(NULL); \
}


#define BombermanTest_Source_BombermanTest_Bomb_h_18_SPARSE_DATA
#define BombermanTest_Source_BombermanTest_Bomb_h_18_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnBombExplosionFireOverlapPlayer);


#define BombermanTest_Source_BombermanTest_Bomb_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnBombExplosionFireOverlapPlayer);


#define BombermanTest_Source_BombermanTest_Bomb_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesABomb(); \
	friend struct Z_Construct_UClass_ABomb_Statics; \
public: \
	DECLARE_CLASS(ABomb, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(ABomb)


#define BombermanTest_Source_BombermanTest_Bomb_h_18_INCLASS \
private: \
	static void StaticRegisterNativesABomb(); \
	friend struct Z_Construct_UClass_ABomb_Statics; \
public: \
	DECLARE_CLASS(ABomb, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(ABomb)


#define BombermanTest_Source_BombermanTest_Bomb_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ABomb(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ABomb) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABomb); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABomb); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABomb(ABomb&&); \
	NO_API ABomb(const ABomb&); \
public:


#define BombermanTest_Source_BombermanTest_Bomb_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ABomb(ABomb&&); \
	NO_API ABomb(const ABomb&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ABomb); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ABomb); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ABomb)


#define BombermanTest_Source_BombermanTest_Bomb_h_18_PRIVATE_PROPERTY_OFFSET
#define BombermanTest_Source_BombermanTest_Bomb_h_15_PROLOG
#define BombermanTest_Source_BombermanTest_Bomb_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_Bomb_h_18_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_Bomb_h_18_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_Bomb_h_18_RPC_WRAPPERS \
	BombermanTest_Source_BombermanTest_Bomb_h_18_INCLASS \
	BombermanTest_Source_BombermanTest_Bomb_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BombermanTest_Source_BombermanTest_Bomb_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_Bomb_h_18_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_Bomb_h_18_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_Bomb_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_Bomb_h_18_INCLASS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_Bomb_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOMBERMANTEST_API UClass* StaticClass<class ABomb>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BombermanTest_Source_BombermanTest_Bomb_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
