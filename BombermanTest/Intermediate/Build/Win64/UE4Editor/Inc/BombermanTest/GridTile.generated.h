// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef BOMBERMANTEST_GridTile_generated_h
#error "GridTile.generated.h already included, missing '#pragma once' in GridTile.h"
#endif
#define BOMBERMANTEST_GridTile_generated_h

#define BombermanTest_Source_BombermanTest_GridTile_h_14_SPARSE_DATA
#define BombermanTest_Source_BombermanTest_GridTile_h_14_RPC_WRAPPERS
#define BombermanTest_Source_BombermanTest_GridTile_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define BombermanTest_Source_BombermanTest_GridTile_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAGridTile(); \
	friend struct Z_Construct_UClass_AGridTile_Statics; \
public: \
	DECLARE_CLASS(AGridTile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(AGridTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_14_INCLASS \
private: \
	static void StaticRegisterNativesAGridTile(); \
	friend struct Z_Construct_UClass_AGridTile_Statics; \
public: \
	DECLARE_CLASS(AGridTile, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(AGridTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AGridTile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AGridTile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGridTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGridTile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGridTile(AGridTile&&); \
	NO_API AGridTile(const AGridTile&); \
public:


#define BombermanTest_Source_BombermanTest_GridTile_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AGridTile(AGridTile&&); \
	NO_API AGridTile(const AGridTile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AGridTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AGridTile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AGridTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_14_PRIVATE_PROPERTY_OFFSET
#define BombermanTest_Source_BombermanTest_GridTile_h_11_PROLOG
#define BombermanTest_Source_BombermanTest_GridTile_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_GridTile_h_14_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_GridTile_h_14_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_GridTile_h_14_RPC_WRAPPERS \
	BombermanTest_Source_BombermanTest_GridTile_h_14_INCLASS \
	BombermanTest_Source_BombermanTest_GridTile_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BombermanTest_Source_BombermanTest_GridTile_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_GridTile_h_14_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_GridTile_h_14_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_GridTile_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_GridTile_h_14_INCLASS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_GridTile_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOMBERMANTEST_API UClass* StaticClass<class AGridTile>();

#define BombermanTest_Source_BombermanTest_GridTile_h_52_SPARSE_DATA
#define BombermanTest_Source_BombermanTest_GridTile_h_52_RPC_WRAPPERS
#define BombermanTest_Source_BombermanTest_GridTile_h_52_RPC_WRAPPERS_NO_PURE_DECLS
#define BombermanTest_Source_BombermanTest_GridTile_h_52_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGridEmptyMeshTile(); \
	friend struct Z_Construct_UClass_UGridEmptyMeshTile_Statics; \
public: \
	DECLARE_CLASS(UGridEmptyMeshTile, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(UGridEmptyMeshTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_52_INCLASS \
private: \
	static void StaticRegisterNativesUGridEmptyMeshTile(); \
	friend struct Z_Construct_UClass_UGridEmptyMeshTile_Statics; \
public: \
	DECLARE_CLASS(UGridEmptyMeshTile, UStaticMeshComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(UGridEmptyMeshTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_52_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGridEmptyMeshTile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGridEmptyMeshTile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridEmptyMeshTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridEmptyMeshTile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridEmptyMeshTile(UGridEmptyMeshTile&&); \
	NO_API UGridEmptyMeshTile(const UGridEmptyMeshTile&); \
public:


#define BombermanTest_Source_BombermanTest_GridTile_h_52_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridEmptyMeshTile(UGridEmptyMeshTile&&); \
	NO_API UGridEmptyMeshTile(const UGridEmptyMeshTile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridEmptyMeshTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridEmptyMeshTile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGridEmptyMeshTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_52_PRIVATE_PROPERTY_OFFSET
#define BombermanTest_Source_BombermanTest_GridTile_h_49_PROLOG
#define BombermanTest_Source_BombermanTest_GridTile_h_52_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_GridTile_h_52_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_GridTile_h_52_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_GridTile_h_52_RPC_WRAPPERS \
	BombermanTest_Source_BombermanTest_GridTile_h_52_INCLASS \
	BombermanTest_Source_BombermanTest_GridTile_h_52_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BombermanTest_Source_BombermanTest_GridTile_h_52_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_GridTile_h_52_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_GridTile_h_52_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_GridTile_h_52_RPC_WRAPPERS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_GridTile_h_52_INCLASS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_GridTile_h_52_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOMBERMANTEST_API UClass* StaticClass<class UGridEmptyMeshTile>();

#define BombermanTest_Source_BombermanTest_GridTile_h_71_SPARSE_DATA
#define BombermanTest_Source_BombermanTest_GridTile_h_71_RPC_WRAPPERS
#define BombermanTest_Source_BombermanTest_GridTile_h_71_RPC_WRAPPERS_NO_PURE_DECLS
#define BombermanTest_Source_BombermanTest_GridTile_h_71_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGridUnbreakableBrickMeshTile(); \
	friend struct Z_Construct_UClass_UGridUnbreakableBrickMeshTile_Statics; \
public: \
	DECLARE_CLASS(UGridUnbreakableBrickMeshTile, UGridEmptyMeshTile, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(UGridUnbreakableBrickMeshTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_71_INCLASS \
private: \
	static void StaticRegisterNativesUGridUnbreakableBrickMeshTile(); \
	friend struct Z_Construct_UClass_UGridUnbreakableBrickMeshTile_Statics; \
public: \
	DECLARE_CLASS(UGridUnbreakableBrickMeshTile, UGridEmptyMeshTile, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(UGridUnbreakableBrickMeshTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_71_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGridUnbreakableBrickMeshTile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGridUnbreakableBrickMeshTile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridUnbreakableBrickMeshTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridUnbreakableBrickMeshTile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridUnbreakableBrickMeshTile(UGridUnbreakableBrickMeshTile&&); \
	NO_API UGridUnbreakableBrickMeshTile(const UGridUnbreakableBrickMeshTile&); \
public:


#define BombermanTest_Source_BombermanTest_GridTile_h_71_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridUnbreakableBrickMeshTile(UGridUnbreakableBrickMeshTile&&); \
	NO_API UGridUnbreakableBrickMeshTile(const UGridUnbreakableBrickMeshTile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridUnbreakableBrickMeshTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridUnbreakableBrickMeshTile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGridUnbreakableBrickMeshTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_71_PRIVATE_PROPERTY_OFFSET
#define BombermanTest_Source_BombermanTest_GridTile_h_68_PROLOG
#define BombermanTest_Source_BombermanTest_GridTile_h_71_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_GridTile_h_71_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_GridTile_h_71_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_GridTile_h_71_RPC_WRAPPERS \
	BombermanTest_Source_BombermanTest_GridTile_h_71_INCLASS \
	BombermanTest_Source_BombermanTest_GridTile_h_71_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BombermanTest_Source_BombermanTest_GridTile_h_71_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_GridTile_h_71_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_GridTile_h_71_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_GridTile_h_71_RPC_WRAPPERS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_GridTile_h_71_INCLASS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_GridTile_h_71_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOMBERMANTEST_API UClass* StaticClass<class UGridUnbreakableBrickMeshTile>();

#define BombermanTest_Source_BombermanTest_GridTile_h_80_SPARSE_DATA
#define BombermanTest_Source_BombermanTest_GridTile_h_80_RPC_WRAPPERS
#define BombermanTest_Source_BombermanTest_GridTile_h_80_RPC_WRAPPERS_NO_PURE_DECLS
#define BombermanTest_Source_BombermanTest_GridTile_h_80_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGridBreakableBrickMeshTile(); \
	friend struct Z_Construct_UClass_UGridBreakableBrickMeshTile_Statics; \
public: \
	DECLARE_CLASS(UGridBreakableBrickMeshTile, UGridEmptyMeshTile, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(UGridBreakableBrickMeshTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_80_INCLASS \
private: \
	static void StaticRegisterNativesUGridBreakableBrickMeshTile(); \
	friend struct Z_Construct_UClass_UGridBreakableBrickMeshTile_Statics; \
public: \
	DECLARE_CLASS(UGridBreakableBrickMeshTile, UGridEmptyMeshTile, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(UGridBreakableBrickMeshTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_80_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGridBreakableBrickMeshTile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGridBreakableBrickMeshTile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridBreakableBrickMeshTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridBreakableBrickMeshTile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridBreakableBrickMeshTile(UGridBreakableBrickMeshTile&&); \
	NO_API UGridBreakableBrickMeshTile(const UGridBreakableBrickMeshTile&); \
public:


#define BombermanTest_Source_BombermanTest_GridTile_h_80_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridBreakableBrickMeshTile(UGridBreakableBrickMeshTile&&); \
	NO_API UGridBreakableBrickMeshTile(const UGridBreakableBrickMeshTile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridBreakableBrickMeshTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridBreakableBrickMeshTile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGridBreakableBrickMeshTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_80_PRIVATE_PROPERTY_OFFSET
#define BombermanTest_Source_BombermanTest_GridTile_h_77_PROLOG
#define BombermanTest_Source_BombermanTest_GridTile_h_80_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_GridTile_h_80_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_GridTile_h_80_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_GridTile_h_80_RPC_WRAPPERS \
	BombermanTest_Source_BombermanTest_GridTile_h_80_INCLASS \
	BombermanTest_Source_BombermanTest_GridTile_h_80_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BombermanTest_Source_BombermanTest_GridTile_h_80_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_GridTile_h_80_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_GridTile_h_80_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_GridTile_h_80_RPC_WRAPPERS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_GridTile_h_80_INCLASS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_GridTile_h_80_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOMBERMANTEST_API UClass* StaticClass<class UGridBreakableBrickMeshTile>();

#define BombermanTest_Source_BombermanTest_GridTile_h_89_SPARSE_DATA
#define BombermanTest_Source_BombermanTest_GridTile_h_89_RPC_WRAPPERS
#define BombermanTest_Source_BombermanTest_GridTile_h_89_RPC_WRAPPERS_NO_PURE_DECLS
#define BombermanTest_Source_BombermanTest_GridTile_h_89_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUGridStartingPointMeshTile(); \
	friend struct Z_Construct_UClass_UGridStartingPointMeshTile_Statics; \
public: \
	DECLARE_CLASS(UGridStartingPointMeshTile, UGridEmptyMeshTile, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(UGridStartingPointMeshTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_89_INCLASS \
private: \
	static void StaticRegisterNativesUGridStartingPointMeshTile(); \
	friend struct Z_Construct_UClass_UGridStartingPointMeshTile_Statics; \
public: \
	DECLARE_CLASS(UGridStartingPointMeshTile, UGridEmptyMeshTile, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/BombermanTest"), NO_API) \
	DECLARE_SERIALIZER(UGridStartingPointMeshTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_89_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UGridStartingPointMeshTile(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UGridStartingPointMeshTile) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridStartingPointMeshTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridStartingPointMeshTile); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridStartingPointMeshTile(UGridStartingPointMeshTile&&); \
	NO_API UGridStartingPointMeshTile(const UGridStartingPointMeshTile&); \
public:


#define BombermanTest_Source_BombermanTest_GridTile_h_89_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UGridStartingPointMeshTile(UGridStartingPointMeshTile&&); \
	NO_API UGridStartingPointMeshTile(const UGridStartingPointMeshTile&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UGridStartingPointMeshTile); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UGridStartingPointMeshTile); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UGridStartingPointMeshTile)


#define BombermanTest_Source_BombermanTest_GridTile_h_89_PRIVATE_PROPERTY_OFFSET
#define BombermanTest_Source_BombermanTest_GridTile_h_86_PROLOG
#define BombermanTest_Source_BombermanTest_GridTile_h_89_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_GridTile_h_89_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_GridTile_h_89_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_GridTile_h_89_RPC_WRAPPERS \
	BombermanTest_Source_BombermanTest_GridTile_h_89_INCLASS \
	BombermanTest_Source_BombermanTest_GridTile_h_89_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define BombermanTest_Source_BombermanTest_GridTile_h_89_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	BombermanTest_Source_BombermanTest_GridTile_h_89_PRIVATE_PROPERTY_OFFSET \
	BombermanTest_Source_BombermanTest_GridTile_h_89_SPARSE_DATA \
	BombermanTest_Source_BombermanTest_GridTile_h_89_RPC_WRAPPERS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_GridTile_h_89_INCLASS_NO_PURE_DECLS \
	BombermanTest_Source_BombermanTest_GridTile_h_89_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> BOMBERMANTEST_API UClass* StaticClass<class UGridStartingPointMeshTile>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID BombermanTest_Source_BombermanTest_GridTile_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
